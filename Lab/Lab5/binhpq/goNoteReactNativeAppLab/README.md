# React Native Lab

## Require:
- Clone project
- Install dependencies using npm or yarn
- Implement TODO list

## Screenshot:
Before:
<p align='center'>
  <span>
      <image width="200" src="https://i.imgur.com/apUBeW7.png" />
      <image width="200" src="https://i.imgur.com/8jOwv1W.png" />
  </span>
</p>
After:
<p align='center'>
  <span>
      <image width="200" src="https://i.imgur.com/apUBeW7.png" />
       <image width="200" src="https://i.imgur.com/jsOVs7z.png" />
  </span>
</p>

## Ref
**Notes App React-Native with Redux**, https://github.com/DwiNugroho/NotesApp-ReactNative-Redux
