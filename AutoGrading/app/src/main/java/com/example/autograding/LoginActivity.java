package com.example.autograding;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.autograding.DTO.User;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.example.autograding.objects.MyUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Timer;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {
    Timer timer;
    private EditText txt_username;
    private EditText txt_password;
    private ImageButton sign_in_btn;
    private ImageView facebook_btn;
    private ImageView gmail_btn;
    private ImageView google_btn;
    private TextView forgot_password;;

    JsonPlaceHolderApi jsonPlaceHolderApi;

    private static final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        sign_in_btn = (ImageButton) findViewById(R.id.button);
        sign_in_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        facebook_btn = (ImageView) findViewById(R.id.facebook_btn);
        facebook_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.facebook.com/bien.nguyen93/")));
            }
        });

        gmail_btn = (ImageView) findViewById(R.id.gmail_btn);
        gmail_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectorIntent = new Intent(Intent.ACTION_SENDTO);
                selectorIntent.setData(Uri.parse("mailto:"));

                Intent email= new Intent(Intent.ACTION_SENDTO);
                email.setData(Uri.parse("bien.nguyenbk00@hcmut.edu.vn"));
                email.putExtra(Intent.EXTRA_SUBJECT, "[Auto Grading] Customer Contact");
                email.putExtra(Intent.EXTRA_TEXT, "Type your message here...");
                email.setSelector(selectorIntent);
                startActivity(email);
            }
        });

        google_btn = (ImageView) findViewById(R.id.google_btn);
        google_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.google.com/search?q=bien.nguyenkb00%40hcmut.edu.vn&ei=V2euYJzoEbvF4-EPx4OE-AE&oq=bien.nguyenkb00%40hcmut.edu.vn&gs_lcp=Cgdnd3Mtd2l6EANQvqoBWL6qAWDVrAFoAHAAeACAAY0BiAH-AZIBAzAuMpgBAKABAaoBB2d3cy13aXrAAQE&sclient=gws-wiz&ved=0ahUKEwjc7-6C0-fwAhW74jgGHccBAR8Q4dUDCA4&uact=5")));
            }
        });

        forgot_password = (TextView) findViewById(R.id.btn_forgetpassword);
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectorIntent = new Intent(Intent.ACTION_SENDTO);
                selectorIntent.setData(Uri.parse("mailto:"));

                Intent email = new Intent(Intent.ACTION_SENDTO);
                email.setData(Uri.parse("bien.nguyenbk00@hcmut.edu.vn"));
                email.putExtra(Intent.EXTRA_SUBJECT, "[Auto Grading] Forgot password request");
                email.putExtra(Intent.EXTRA_TEXT,
                          "Your username:\n" +
                                "Your student ID:\n" +
                                "Your birthday:\n" +
                                "Your phone number:\n" +
                                "Your identity card number:\n" +
                                "Your date of issuance of identity card:");
                email.setSelector(selectorIntent);
                startActivity(email);
            }
        });
    }

    private void signIn(){

        txt_username = (EditText) findViewById(R.id.username);
        txt_password = (EditText) findViewById(R.id.password);
        String username,password;
        username=txt_username.getText().toString();
        password=txt_password.getText().toString();

        HashMap<String,String> jsonParams = new HashMap<>();
        jsonParams.put("username",username);
        jsonParams.put("password",password);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(new JSONObject(jsonParams)).toString());
        Call<ResponseBody> call = jsonPlaceHolderApi.signIn(requestBody);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    Toast toast = Toast.makeText(LoginActivity.this,"Wrong username or password", Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    boolean is_teacher = (boolean) jsonObject.get("is_teacher");
                    int user_id = (int) jsonObject.get("user_id");
                    User.getInstance().setUserID(user_id);
                    if (is_teacher) {
                        Intent intent = new Intent(LoginActivity.this, HomeTeacherActivity.class);
                        intent.putExtra("user_id",String.valueOf(user_id));
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Intent intent = new Intent(LoginActivity.this, HomeStudentActivity.class);
                        intent.putExtra("user_id",String.valueOf(user_id));
                        startActivity(intent);
                        finish();
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                txt_username.setText(t.getMessage());
            }
        });
    }




}