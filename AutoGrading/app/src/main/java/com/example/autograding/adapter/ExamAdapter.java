package com.example.autograding.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.autograding.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ExamAdapter extends BaseAdapter {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    public static final String EXAM_DATE = "1";
    public static final String EXAM_DURATION = "2";
    public  static  final  String EXAM_NAME ="3";
    public  static  final  String EXAM_ID= "4";


    public ExamAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        TextView tvExamName;
        TextView tvExamDuration;
        TextView tvExamDate;
        TextView tvExamID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ExamAdapter.ViewHolder holder;

        LayoutInflater inflater = activity.getLayoutInflater();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.object_exam, null);
            holder = new ExamAdapter.ViewHolder();
            holder.tvExamID = (TextView) convertView.findViewById(R.id.lb_examID);
            holder.tvExamName = (TextView) convertView.findViewById(R.id.lb_examName);
            holder.tvExamDate= (TextView) convertView.findViewById(R.id.lb_date_exam);
            holder.tvExamDuration =(TextView) convertView.findViewById(R.id.lb_examDuration);
            convertView.setTag(holder);
        } else {
            holder = (ExamAdapter.ViewHolder) convertView.getTag();
        }

        HashMap<String, String> map = list.get(position);
        holder.tvExamDuration.setText(map.get(EXAM_DURATION)+ " minutes");
        holder.tvExamDate.setText(map.get(EXAM_DATE));
        holder.tvExamID.setText("Exam no. " + map.get(EXAM_ID));
        holder.tvExamName.setText(map.get(EXAM_NAME));

        return convertView;
    }
}

