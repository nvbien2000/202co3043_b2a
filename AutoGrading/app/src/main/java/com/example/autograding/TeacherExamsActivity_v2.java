package com.example.autograding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.autograding.FragmentTeacherExams.FragmentExamsCode;
import com.example.autograding.FragmentTeacherExams.FragmentExamsText;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class TeacherExamsActivity_v2 extends AppCompatActivity {
     TabLayout tabLayout;
     ViewPager viewPager;
     TextView tvNameCourse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_exams_v2);

        tabLayout = findViewById(R.id.TabPager);
        viewPager = findViewById(R.id.viewPager);
        tvNameCourse = findViewById(R.id.tv_nameCourse);

        Bundle extras = getIntent().getExtras();
        tvNameCourse.setText(extras.getString("subject_name"));

        getTab();

        //back button clicked
        ImageButton btnBack= findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }
    private  void getTab()
    {
        final  ViewPagerAdapter viewPagerAdapter= new ViewPagerAdapter(getSupportFragmentManager());
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ViewPagerAdapter viewPagerAdapter =new ViewPagerAdapter(getSupportFragmentManager());
                viewPagerAdapter.addFragment(FragmentExamsCode.getInstance(),"Coding Exam");
                viewPagerAdapter.addFragment(FragmentExamsText.getInstance(),"Multichoice Exam");
                viewPager.setAdapter(viewPagerAdapter);
                tabLayout.setupWithViewPager(viewPager);
            }
        });

    }

    private  class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private List<Fragment> fragments= new ArrayList<>();
        private List<String> stringList= new ArrayList<>();
        public ViewPagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {

            return stringList.get(position);
        }

        private void addFragment(Fragment fragment, String title)
        {
            fragments.add(fragment);
            stringList.add(title);
        }
    }

    public int getUserId() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int user_id = Integer.parseInt(extras.getString("user_id"));
            return user_id;
        }
        return -1;
    }
    public int getCourseId() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int course_id = Integer.parseInt(extras.getString("course_id"));
            return course_id;
        }
        return -1;
    }
    public String getCourseName() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String subject_name = extras.getString("subject_name");
            return subject_name;
        }
        return "";
    }
}