package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.autograding.adapter.ExamAdapter;
import com.example.autograding.adapter.ExamAdapterCopyBien;
import com.example.autograding.adapter.UpcomingExamAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.example.autograding.objects.UpcomingExamDTO;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class studentExamActivity extends AppCompatActivity {
    private ArrayList<HashMap<String,String>> list;
    public static final String EXAM_DATE = "1";
    public static final String EXAM_DURATION = "2";
    public static final String EXAM_NAME ="3";
    public static final String EXAM_ID= "4";
    private int user_id;
    ExamAdapter adapter;
    ListView listView;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    List<UpcomingExamDTO> upcomingExams= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_exam);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        user_id= getIntent().getIntExtra("user_id",1);
        getUpcomingExam();
        listView = (ListView) findViewById(R.id.lv_exams);
//        ExamAdapter adapter = new ExamAdapter(this, list);
//        listView.setAdapter(adapter);

        ImageButton btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


    }
    private void getUpcomingExam()
    {
        list = new ArrayList<HashMap<String, String>>();
        upcomingExams.clear();
        Call<ResponseBody> call = jsonPlaceHolderApi.get_upcomming_exam(user_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray courses = (JSONArray) jsonObject.get("result");

                    for( int i=0;i< courses.length();i++)
                    {
                        JSONObject course = courses.getJSONObject(i);
                        String name= (String) course.get("name");
                        UpcomingExamDTO t= new UpcomingExamDTO(course.getInt("course_id"), course.getInt("id"), course.get("subject_id").toString() , name,course.get("full_date").toString(),course.getInt("remaining_day"),course.getInt("remaining_hour"),course.getInt("remaining_min"), course.getInt("length"));

                        HashMap<String,String> map = new HashMap<String,String>();
                        map.put(EXAM_DATE, (String) course.get("full_date"));
                        map.put(EXAM_DURATION,    course.get("length").toString());
                        map.put(EXAM_ID, (String) course.get("subject_id"));
                        map.put(EXAM_NAME, (String) course.get("name"));
                        list.add(map);
                        upcomingExams.add(t);
                    }


                    adapter = new ExamAdapter(studentExamActivity.this, list);
                    listView.setAdapter(adapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent= new Intent(studentExamActivity.this, CheckInActivity.class);
                            intent.putExtra("exam_info", upcomingExams.get(position).getNameExam());
                            intent.putExtra("length",upcomingExams.get(position).getDuration());
                            intent.putExtra("subject_id",upcomingExams.get(position).getSubject_id());
                            intent.putExtra("exam_id",upcomingExams.get(position).getExam_id());
                            startActivity(intent);
                        }
                    });
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });




    }
    private void populateList() {
        list = new ArrayList<HashMap<String, String>>();
        HashMap<String,String> hashmap = new HashMap<String,String>();
        hashmap.put(EXAM_ID, "CO1005");
        hashmap.put(EXAM_NAME, "Aaaaaaaaaaa");
        hashmap.put(EXAM_DATE, "2:00 PM 31/12/2021");
        hashmap.put(EXAM_DURATION, "120 mins");
        list.add(hashmap);

        HashMap<String,String> hashmap1 = new HashMap<String,String>();
        hashmap1.put(EXAM_ID, "CO1006");
        hashmap1.put(EXAM_NAME, "Bbbbbbbbbbb");
        hashmap1.put(EXAM_DATE, "12:00 PM 12/12/2021");
        hashmap1.put(EXAM_DURATION, "120 mins");
        list.add(hashmap1);

        HashMap<String,String> hashmap2 = new HashMap<String,String>();
        hashmap2.put(EXAM_ID, "CO1007");
        hashmap2.put(EXAM_NAME, "Cccccccc");
        hashmap2.put(EXAM_DATE, "21:00 PM 31/12/2021");
        hashmap2.put(EXAM_DURATION, "20 mins");
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
    }
}
