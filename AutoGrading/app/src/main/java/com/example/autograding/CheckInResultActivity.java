package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.autograding.DTO.User;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.example.autograding.utils.Common;
import com.example.autograding.utils.IUploadCallbacks;
import com.example.autograding.utils.ProgressRequestBody;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CheckInResultActivity extends AppCompatActivity implements IUploadCallbacks {

    ImageView imageView;
    ImageButton btnUpload;
    ImageButton btnCancel;
    JsonPlaceHolderApi mService;
    Uri selectedFileUri;
    ProgressDialog dialog;
    int exam_id;
    String subject_id;
    String exam_info;
    User myuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        myuser = User.getInstance();

        Bundle extras = getIntent().getExtras();
        exam_id = getIntent().getIntExtra("exam_id",0);
        subject_id= getIntent().getStringExtra("subject_id");
        exam_info=   getIntent().getStringExtra("exam_info");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mService = retrofit.create(JsonPlaceHolderApi.class);

        btnUpload = findViewById(R.id.btn_ok);
        btnCancel = findViewById(R.id.btn_cancel);

        imageView = findViewById(R.id.iv_scanimg);
        Uri image_uri = getIntent().getData();
        imageView.setImageURI(image_uri);
        selectedFileUri = image_uri;
        uploadFile();
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckInResultActivity.this, CodeExamTypeV2 .class);
                intent.putExtra("subject_id", subject_id);
                intent.putExtra("exam_name",exam_info);
                intent.putExtra("exam_id", getIntent().getIntExtra("exam_id",0));
                intent.putExtra("course_id",getIntent().getIntExtra("course_id",0));
                startActivity(intent);
                finish();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckInResultActivity.this, CheckInActivity .class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void uploadFile() {
        if (selectedFileUri != null) {
            dialog = new ProgressDialog(CheckInResultActivity.this);
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setMessage("Uploading...");
            dialog.setIndeterminate(false);
            dialog.setMax(1);
            dialog.setCancelable(false);
            dialog.show();

            File file = null;
            try {
                file = new File(Common.getFilePath(this,selectedFileUri));
            }
            catch (URISyntaxException e) {
                e.printStackTrace();
            }
            if (file != null) {
                final ProgressRequestBody requestBody = new ProgressRequestBody(this,file);
                final MultipartBody.Part body = MultipartBody.Part.createFormData("image",file.getName(),requestBody);
                Call<ResponseBody> call = mService.checkin(exam_id, myuser.getUserID(), getIntent().getStringExtra("location"),  getIntent().getStringExtra("ip"), body);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String jsonData = response.body().string();
                            JSONObject jsonObject = new JSONObject(jsonData);
                            String image_link = (String) jsonObject.get("image_url");
                            String image_processed_link = new StringBuilder(image_link.replace("\"","")).toString();
                            Toast.makeText(CheckInResultActivity.this, "Please wait, Image is processing...", Toast.LENGTH_SHORT).show();
                            Picasso.get()
                                    .load(image_processed_link)
                                    .fit().centerInside()
                                    .into(imageView);
                            dialog.dismiss();
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(CheckInResultActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        else {
            Toast.makeText(this, "Cannot upload this file...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onProgressUpdate(int present) {

    }
}