package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.autograding.DTO.User;
import com.example.autograding.adapter.ExamAdapterCopyBien;
import com.example.autograding.adapter.UpcomingExamAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.example.autograding.objects.UpcomingExamDTO;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeStudentActivity extends AppCompatActivity {
    ViewPager viewPager;
    UpcomingExamAdapter upcomingExamAdapter;
    List<UpcomingExamDTO> upcomingExams;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    private ArrayList<HashMap<String,String>> list;
    private int user_id;
    public static final String SUBJECT_TEACHER = "1";
    public static final String SUBJECT_NAME ="2";
    public static final String SUBJECT_ID = "3";

    private TextView tv_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_student);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        upcomingExams= new ArrayList<>();
        viewPager = findViewById(R.id.viewPagerUpcommingExam);
        tv_name= findViewById(R.id.tv_name);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
              user_id = Integer.parseInt(extras.getString("user_id"));
            getCourse(user_id);
            getUpComingExam(user_id);
            getName(user_id);
        }



        // grade detail adapter
//        upcomingExams= new ArrayList<>();
//        upcomingExams.add(new UpcomingExamDTO("Your Exam1 is scheduled on","21 APRIL 2021",1,1,10));
//        upcomingExams.add(new UpcomingExamDTO("Your Exam2 is scheduled on","22 APRIL 2021",2,2,20));
//        upcomingExams.add(new UpcomingExamDTO("Your Exam3 is scheduled on","23 APRIL 2021",4 ,4,30));
//        upcomingExams.add(new UpcomingExamDTO("Your Exam4 is scheduled on","24 APRIL 2021",8 ,8,40));
//        upcomingExams.add(new UpcomingExamDTO("Your Exam5 is scheduled on","25 APRIL 2021",16 ,16,50));




        FloatingActionButton home_btn = findViewById(R.id.home_btn);
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeStudentActivity.this, HomeStudentActivity.class);
                startActivity(intent);

            }
        });

        TextView btn_view_all_exams = findViewById(R.id.btn_view_all_exams);
        btn_view_all_exams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeStudentActivity.this, studentExamActivity.class);
                intent.putExtra("user_id", user_id);
                startActivity(intent);
            }
        });

        BottomAppBar bar = findViewById(R.id.bottomAppBar);
        setSupportActionBar(bar);
    }

    private void getName(int user_id)
    {
        Call<ResponseBody> call = jsonPlaceHolderApi.get_info(user_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject= new JSONObject(jsonData);
                    tv_name.setText("Hi, "+jsonObject.getString("name"));
                    User.getInstance().setFullName(jsonObject.getString("full_name"));
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                Intent intent = new Intent(HomeStudentActivity.this, HomeTeacherActivity.class);
                return true;
            case R.id.menuUser:
                intent = new Intent(HomeStudentActivity.this, InfomationActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getUpComingExam(int user_id)
    {
        upcomingExams.clear();
        Call<ResponseBody> call = jsonPlaceHolderApi.get_upcomming_exam(user_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray courses = (JSONArray) jsonObject.get("result");

                    for( int i=0;i< courses.length();i++)
                    {
                        JSONObject course = courses.getJSONObject(i);
                        String name= (String) course.get("name");
                        UpcomingExamDTO t= new UpcomingExamDTO(course.getInt("course_id"), course.getInt("id"), course.get("subject_id").toString() , name,course.get("exam_date").toString(),course.getInt("remaining_day"),course.getInt("remaining_hour"),course.getInt("remaining_min"), course.getInt("length"));
                        upcomingExams.add(t);
                    }

                    upcomingExamAdapter = new UpcomingExamAdapter(upcomingExams,HomeStudentActivity.this);

                    viewPager.setAdapter(upcomingExamAdapter);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });



        upcomingExamAdapter = new UpcomingExamAdapter(upcomingExams,this);
        viewPager = findViewById(R.id.viewPagerUpcommingExam);
        viewPager.setAdapter(upcomingExamAdapter);
    }
    private void getCourse(int user_id) {
        list = new ArrayList<HashMap<String, String>>();
        Call<ResponseBody> call = jsonPlaceHolderApi.getHomeStudent(user_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray courses = (JSONArray) jsonObject.get("results");
                    for (int i = 0; i < courses.length(); i++) {
                        JSONObject course = courses.getJSONObject(i);
                        HashMap<String,String> map = new HashMap<String,String>();
                        map.put(SUBJECT_TEACHER, (String) course.get("teacher_name"));
                        map.put(SUBJECT_ID, (String) course.get("subject_name"));
                        map.put(SUBJECT_NAME, (String) course.get("subject_id"));
                        list.add(map);
                    }
                    ListView listView = findViewById(R.id.lv_courses);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(HomeStudentActivity.this,welcomClassActiviy.class);
                            intent.putExtra("subject_id",list.get(position).get(SUBJECT_NAME));
                            intent.putExtra("subject_name",list.get(position).get(SUBJECT_ID));

                            startActivity(intent);
                        }
                    });

                    ExamAdapterCopyBien adapter = new ExamAdapterCopyBien(HomeStudentActivity.this, list);
                    listView.setAdapter(adapter);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }




    private List<Integer> formatRemainingTime(String datetime)
    {
        List<Integer> lst= new ArrayList<>();
        String day,hour,min;
        if (datetime.charAt(6)=='s')
        {
            day= datetime.charAt(0)+datetime.charAt(1)+"";
        }

        return lst;
    }
}
