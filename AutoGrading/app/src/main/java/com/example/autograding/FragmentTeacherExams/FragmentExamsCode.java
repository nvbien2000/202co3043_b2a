package com.example.autograding.FragmentTeacherExams;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.autograding.CodeSheetsActivity;
import com.example.autograding.CreateExamActivity;
import com.example.autograding.HomeTeacherActivity;
import com.example.autograding.R;
import com.example.autograding.ScannedTaskActivity;
import com.example.autograding.TeacherExamsActivity_v2;
import com.example.autograding.adapter.ExamAdapter;
import com.example.autograding.adapter.ExamAdapterCopyBien;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.example.autograding.teacherExamsActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FragmentExamsCode extends Fragment {
    int user_id;
    private ArrayList<HashMap<String,String>> list;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    public static final String EXAM_DATE = "1";
    public static final String EXAM_DURATION = "2";
    public static final String EXAM_NAME ="3";
    public static final String EXAM_ID= "4";
    FloatingActionButton btn_addExam;
    String course_name;
    public  static FragmentExamsCode getInstance()
    {
        FragmentExamsCode fragmentExamsCode= new FragmentExamsCode();
        return  fragmentExamsCode;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_teacher_exams_code,container,false);
        TeacherExamsActivity_v2 activity = (TeacherExamsActivity_v2) getActivity();
        int course_id = activity.getCourseId();
        course_name = activity.getCourseName();
        user_id = activity.getUserId();
        btn_addExam = view.findViewById(R.id.btn_add_exam);
        btn_addExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), CreateExamActivity.class);
                i.putExtra("subject_name", activity.getCourseName());
                i.putExtra("course_id", String.valueOf(course_id));
                i.putExtra("user_id",String.valueOf(user_id));
                startActivity(i);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        getExams(view, course_id);
        return  view;
    }

    private void getExams(View view, int course_id) {
        list = new ArrayList<HashMap<String, String>>();
        Call<ResponseBody> call = jsonPlaceHolderApi.getCodeExam(course_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray exams = (JSONArray) jsonObject.get("results");
                    for (int i = 0; i < exams.length(); i++) {
                        JSONObject exam = exams.getJSONObject(i);
                        HashMap<String,String> map = new HashMap<String,String>();
                        map.put(EXAM_DATE, (String) exam.get("exam_date"));
                        map.put(EXAM_DURATION, String.valueOf(exam.get("exam_length")));
                        map.put(EXAM_NAME, (String) exam.get("exam_name"));
                        map.put(EXAM_ID, String.valueOf(exam.get("exam_id")));
                        list.add(map);
                    }
                    ListView listView = view.findViewById(R.id.listViewExamCode);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(getActivity(), CodeSheetsActivity.class);
                            intent.putExtra("subject_name", course_name);
                            intent.putExtra("course_id", String.valueOf(course_id));
                            intent.putExtra("exam_id", list.get(position).get(EXAM_ID));
                            intent.putExtra("user_id",String.valueOf(user_id));
                            startActivity(intent);
                        }
                    });

                    ExamAdapter adapter = new ExamAdapter(getActivity(), list);
                    listView.setAdapter(adapter);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }
}
