package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.autograding.adapter.ExamAdapter;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;


public class teacherExamsActivity extends AppCompatActivity {

    private ArrayList<HashMap<String,String>> list;
    public static final String EXAM_DATE = "1";
    public static final String EXAM_DURATION = "2";
    public static final String EXAM_NAME ="3";
    public static final String EXAM_ID= "4";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_exams);

        ImageButton btn_back= (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        populateList();

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(teacherExamsActivity.this,ScannedTaskActivity.class);
                startActivity(intent);
            }
        });

        FloatingActionButton home_btn = findViewById(R.id.home_btn);
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(teacherExamsActivity.this, HomeTeacherActivity.class);
                startActivity(intent);
                finish();
            }
        });

        BottomAppBar bar = findViewById(R.id.bottomAppBar);
        setSupportActionBar(bar);

        Button btn_create_exam= (Button) findViewById(R.id.btn_add_exam) ;
        btn_create_exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getApplicationContext(),CreateExamActivity.class);
                startActivity(intent);
            }
        });
        ExamAdapter adapter = new ExamAdapter(this, list);
        listView.setAdapter(adapter);

    }

    private void populateList() {
        list = new ArrayList<HashMap<String, String>>();
        HashMap<String,String> hashmap = new HashMap<String,String>();
        hashmap.put(EXAM_ID, "CO1005");
        hashmap.put(EXAM_NAME, "Aaaaaaaaaaa");
        hashmap.put(EXAM_DATE, "2:00 PM 31/12/2021");
        hashmap.put(EXAM_DURATION, "120 mins");
        list.add(hashmap);

        HashMap<String,String> hashmap1 = new HashMap<String,String>();
        hashmap1.put(EXAM_ID, "CO1006");
        hashmap1.put(EXAM_NAME, "Bbbbbbbbbbb");
        hashmap1.put(EXAM_DATE, "12:00 PM 12/12/2021");
        hashmap1.put(EXAM_DURATION, "120 mins");
        list.add(hashmap1);

        HashMap<String,String> hashmap2 = new HashMap<String,String>();
        hashmap2.put(EXAM_ID, "CO1007");
        hashmap2.put(EXAM_NAME, "Cccccccc");
        hashmap2.put(EXAM_DATE, "21:00 PM 31/12/2021");
        hashmap2.put(EXAM_DURATION, "20 mins");
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                Intent intent = new Intent(teacherExamsActivity.this, HomeTeacherActivity.class);
                return true;
            case R.id.menuUser:
                intent = new Intent(teacherExamsActivity.this, InfomationActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}