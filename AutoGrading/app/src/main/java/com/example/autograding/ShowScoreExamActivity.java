package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.autograding.DTO.User;
import com.example.autograding.adapter.GradeDetailAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.example.autograding.objects.GradeDetailDTO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ShowScoreExamActivity extends AppCompatActivity {
    ViewPager viewPager;
    GradeDetailAdapter gradeDetailAdapter;
    private View layout_score;
    private Button btnBackHome;
    private TextView tv_examName;
    List<GradeDetailDTO> gradeDetails = new ArrayList<>();
    JsonPlaceHolderApi jsonPlaceHolderApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_score_exam);

        int exam_id= getIntent().getIntExtra("exam_id",0);
        String exam_name= getIntent().getStringExtra("exam_name");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        viewPager= findViewById(R.id.viewPagerGradeDetail);


        LoadScore(exam_id);
        btnBackHome= findViewById(R.id.btnGoHome);
        btnBackHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ShowScoreExamActivity.this,HomeStudentActivity.class));
                finish();
            }
        });
    }

    private void LoadScore(int exam_id)
    {
        gradeDetails.clear();
        Call<ResponseBody> call = jsonPlaceHolderApi.get_score_examcode__did(User.getInstance().getUserID());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray scores = (JSONArray) jsonObject.get("result");
                    for (int i=0;i<scores.length();i++)
                    {
                        JSONObject course = scores.getJSONObject(i);
                        if (course.getInt("exam_id")==exam_id)
                        {
                            gradeDetails.add(new GradeDetailDTO("1",course.getString("exam_name"),course.getInt("length"),course.getInt("score:"),100,course.getInt("avg"),course.getInt("topper")));
                        }
                    }

                    gradeDetailAdapter = new GradeDetailAdapter(gradeDetails,ShowScoreExamActivity.this);
                    viewPager.setAdapter(gradeDetailAdapter);

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}