package com.example.autograding.DTO;

public class User {
    private String username;
    private String password;
    private int userID;
    private int current_exam_id=-1;
    public  int STUDENT=0;
    public  int TEACHER=1;
    private int typeAccount=99;
    private String fullName;
    private static  User instance;
    private User()
    {
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setCurrent_exam_id(int current_exam_id) {
        this.current_exam_id = current_exam_id;
    }

    public int getCurrent_exam_id() {
        return current_exam_id;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getUserID() {
        return userID;
    }

    public static User getInstance()
    {
        if (instance ==null)
        {
            instance =new User();
        }
        return instance;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTypeAccount(int typeAccount) {
        this.typeAccount = typeAccount;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getTypeAccount() {
        return typeAccount;
    }
}
