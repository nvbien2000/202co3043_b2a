package com.example.autograding.objects;

public class UpcomingExamDTO {
    private int course_id;
    private int exam_id;
    private String nameExam;
    private String examDate;
    private int duration;
    private int myDay;
    private int myHour;
    private int myMinute;
    private String subject_id;


    public UpcomingExamDTO(int course_id, int exam_id,String subject_id,  String nameExam, String examDate, int myDay, int myHour, int myMinute, int duration) {
        this.course_id= course_id;
        this.exam_id=exam_id;
        this.nameExam = nameExam;
        this.examDate = examDate;
        this.myDay = myDay;
        this.myHour = myHour;
        this.myMinute = myMinute;
        this.duration=duration;
        this.subject_id=subject_id;
    }

    public int getDuration() {
        return duration;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public int getCourse_id() {
        return course_id;
    }

    public int getExam_id() {
        return exam_id;
    }

    public int getMyDay() {
        return myDay;
    }
    public int getMyHour() {
        return myHour;
    }
    public int getMyMinute() {
        return myMinute;
    }
    public String getNameExam() {
        return nameExam;
    }
    public String getExamDate() {
        return examDate;
    }
}
