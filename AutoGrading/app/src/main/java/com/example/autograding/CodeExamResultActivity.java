package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.autograding.adapter.CodeResultAdapter;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;

public class CodeExamResultActivity extends AppCompatActivity {

    private ArrayList<HashMap<String,String>> list;
    public static final String FIRST_COLUMN = "First";
    public static final String SECOND_COLUMN = "Second";
    public static final String THIRD_COLUMN = "Third";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_exam_result);

        populateList();

        ListView listView = (ListView) findViewById(R.id.listView);
        CodeResultAdapter adapter = new CodeResultAdapter(this, list);
        listView.setAdapter(adapter);

        Button check_btn = (Button) findViewById(R.id.check_btn);
        check_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CodeExamResultActivity.this, HomeStudentActivity.class);
                startActivity(intent);
            }
        });

        ImageButton btn_back= (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Button cancel_btn = (Button) findViewById(R.id.cancel_btn);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CodeExamResultActivity.this, CodeExamTypeActivity.class);
                startActivity(intent);
            }
        });


        Button lastVersion_btn = (Button) findViewById(R.id.lastVersion_btn);
        lastVersion_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CodeExamResultActivity.this, CodeExamTypeActivity.class);
                startActivity(intent);
            }
        });


    FloatingActionButton home_btn = findViewById(R.id.home_btn);
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CodeExamResultActivity.this, HomeStudentActivity.class);
                startActivity(intent);
                finish();
            }
        });

        BottomAppBar bar = findViewById(R.id.bottomAppBar);
        setSupportActionBar(bar);
    }



    private void populateList() {
        list = new ArrayList<HashMap<String, String>>();
        HashMap<String,String> hashmap = new HashMap<String,String>();
        hashmap.put(FIRST_COLUMN, "-");
        hashmap.put(SECOND_COLUMN, "-");
        hashmap.put(THIRD_COLUMN, "-");
        list.add(hashmap);

        HashMap<String,String> hashmap1 = new HashMap<String,String>();
        hashmap1.put(FIRST_COLUMN, "-");
        hashmap1.put(SECOND_COLUMN, "-");
        hashmap1.put(THIRD_COLUMN, "-");
        list.add(hashmap1);

        HashMap<String,String> hashmap2 = new HashMap<String,String>();
        hashmap2.put(FIRST_COLUMN, "-");
        hashmap2.put(SECOND_COLUMN, "-");
        hashmap2.put(THIRD_COLUMN, "-");
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
        list.add(hashmap2);
    }
}