package com.example.autograding;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.autograding.adapter.ScannedTaskAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ScannedTaskActivity extends AppCompatActivity {
    int user_id;
    FloatingActionButton fab_add, fab_export, fab_scan, fab_answer;
    Animation fabOpen, fabClose, fabClockwise, fabAntiClockwise;

    boolean isOpen = false;
    String key;
    int exam_id;
    int sheet_id;

    int count0 = 0;
    int count1 = 0;
    int count2 = 0;
    int count3 = 0;

    TextView tvC0, tvC1, tvC2, tvC3;
    LinearLayout bar0, bar1, bar2, bar3;
    LinearLayout.LayoutParams bar_par0, bar_par1, bar_par2, bar_par3;

    JsonPlaceHolderApi jsonPlaceHolderApi;
    String subject_name;
    private ArrayList<HashMap<String,String>> list;
    public static final String FIRST_COLUMN = "First";
    public static final String SECOND_COLUMN = "Second";
    public static final String THIRD_COLUMN = "Third";

    private static final int PERMISSION_CODE = 1000;
    private static final int IMAGE_CAPTURE_CODE = 1001;
    Uri imageUri;
    StringBuilder data = new StringBuilder();

    TextView tvNameCourse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanned_task);

        tvNameCourse = findViewById(R.id.tv_nameCourse);

        Bundle extras = getIntent().getExtras();
        user_id = Integer.parseInt(extras.getString("user_id"));
        subject_name = extras.getString("subject_name");
        tvNameCourse.setText(subject_name);
        exam_id = Integer.parseInt(extras.getString("exam_id"));

        tvC0 = findViewById(R.id.tv_count0);
        tvC1 = findViewById(R.id.tv_count1);
        tvC2 = findViewById(R.id.tv_count2);
        tvC3 = findViewById(R.id.tv_count3);

        bar0 = findViewById(R.id.bar0);
        bar1 = findViewById(R.id.bar1);
        bar2 = findViewById(R.id.bar2);
        bar3 = findViewById(R.id.bar3);
        bar_par0 = (LinearLayout.LayoutParams) bar0.getLayoutParams();
        bar_par1 = (LinearLayout.LayoutParams) bar1.getLayoutParams();
        bar_par2 = (LinearLayout.LayoutParams) bar2.getLayoutParams();
        bar_par3 = (LinearLayout.LayoutParams) bar3.getLayoutParams();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        getSheets(exam_id);

        FloatingActionButton home_btn = findViewById(R.id.home_btn);
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScannedTaskActivity.this, HomeTeacherActivity.class);
                intent.putExtra("user_id",String.valueOf(user_id));
                startActivity(intent);
                finish();
            }
        });

        ImageButton btn_back= (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        BottomAppBar bar = findViewById(R.id.bottomAppBar);
        setSupportActionBar(bar);

        fab_add = findViewById(R.id.floatingActionButton);
        fab_scan = findViewById(R.id.floatingActionButton1);
        fab_export = findViewById(R.id.floatingActionButton2);
        fab_answer = findViewById(R.id.floatingActionButton3);

        fabOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fabClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        fabClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
        fabAntiClockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_anticlockwise);

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOpen) {
                    fab_scan.startAnimation(fabClose);
                    fab_export.startAnimation(fabClose);
                    fab_answer.startAnimation(fabClose);
                    fab_add.startAnimation(fabClockwise);

                    fab_export.setClickable(false);
                    fab_scan.setClickable(false);
                    fab_answer.setClickable(false);

                    isOpen = false;
                }
                else {
                    fab_scan.startAnimation(fabOpen);
                    fab_export.startAnimation(fabOpen);
                    fab_answer.startAnimation(fabOpen);
                    fab_add.startAnimation(fabAntiClockwise);

                    fab_export.setClickable(true);
                    fab_scan.setClickable(true);
                    fab_answer.setClickable(true);

                    isOpen = true;
                }
            }
        });

        fab_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                key = "0";
                check(v);
            }
        });

        fab_answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                key = "1";
                check(v);
            }
        });

        fab_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.insert(0,"Subject:," + subject_name + "\n");
                export(v);
            }
        });
    }

    public void check(View v) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                requestPermissions(permissions, PERMISSION_CODE);
            }
            else {
                OpenCamera();
            }
        }
        else {
            OpenCamera();
        }
    }

    private void OpenCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE,"New Image");
        values.put(MediaStore.Images.Media.DESCRIPTION,"from the camera");
        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Intent i = new Intent(ScannedTaskActivity.this, ScanActivity.class);
            i.setData(imageUri);
            i.putExtra("key", key);
            i.putExtra("exam_id", String.valueOf(exam_id));
            i.putExtra("subject_name", subject_name);
            startActivity(i);
        }
    }

    private void getSheets(int exam_id) {
        list = new ArrayList<HashMap<String, String>>();
        Call<ResponseBody> call = jsonPlaceHolderApi.getSheets(exam_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray sheets = (JSONArray) jsonObject.get("results");
                    for (int i = 0; i < sheets.length(); i++) {
                        JSONObject sheet = sheets.getJSONObject(i);
                        HashMap<String,String> map = new HashMap<String,String>();
                        map.put(FIRST_COLUMN, (String) sheet.get("student_name"));
                        map.put(SECOND_COLUMN, String.valueOf(sheet.get("score")));
                        map.put(THIRD_COLUMN, String.valueOf(sheet.get("sheet_id")));
                        sheet_id = (Integer) sheet.get("sheet_id");
                        getCSV(sheet_id);
                        Log.d("sheet_id", String.valueOf(sheet_id));

                        if ((Double) sheet.get("score") < 3.0) {
                            count0 += 1;
                        }
                        else if ((Double) sheet.get("score") < 5.0) {
                            count1 += 1;
                        }
                        else if ((Double) sheet.get("score") < 8.0) {
                            count2 += 1;
                        }
                        else {
                            count3 += 1;
                        }
                        list.add(map);
                    }
                    ListView listView = findViewById(R.id.listView);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(ScannedTaskActivity.this, ScancheckActivity.class);
                            intent.putExtra("sheet_id", list.get(position).get(THIRD_COLUMN));
                            intent.putExtra("subject_name", subject_name);
                            intent.putExtra("user_id",String.valueOf(user_id));
                            startActivity(intent);
                        }
                    });


                    ScannedTaskAdapter adapter = new ScannedTaskAdapter(ScannedTaskActivity.this, list);
                    listView.setAdapter(adapter);
                    tvC0.setText(String.valueOf(count0));
                    tvC1.setText(String.valueOf(count1));
                    tvC2.setText(String.valueOf(count2));
                    tvC3.setText(String.valueOf(count3));
                    int sum = count0 + count1 + count2 + count3;
                    if (sum == 0) { sum=1; }
                    int height0 = count0/sum * 300;
                    int height1 = count1/sum * 300;
                    int height2 = count2/sum * 300;
                    int height3 = count3/sum * 300;
                    bar_par0.height = height0; bar0.setLayoutParams(bar_par0);
                    bar_par1.height = height1; bar1.setLayoutParams(bar_par1);
                    bar_par2.height = height2; bar2.setLayoutParams(bar_par2);
                    bar_par3.height = height3; bar3.setLayoutParams(bar_par3);

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public void export(View view){
        //generate data
        Log.d("export_data", data.toString());

        try{
            //saving the file into device
            FileOutputStream out = openFileOutput("data.csv", Context.MODE_PRIVATE);
            out.write((data.toString()).getBytes());
            out.close();

            //exporting
            Context context = getApplicationContext();
            File fileLocation = new File(getFilesDir(), "data.csv");
            Uri path = FileProvider.getUriForFile(context, "com.example.autograding.fileprovider", fileLocation);


            Intent fileIntent = new Intent(Intent.ACTION_SEND);
            fileIntent.setType("text/csv");
            fileIntent.putExtra(Intent.EXTRA_SUBJECT, "Data");
            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            fileIntent.putExtra(Intent.EXTRA_STREAM, path);
            Intent chooser = Intent.createChooser(fileIntent, "Send mail");
            List<ResolveInfo> resInfoList = this.getPackageManager().queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                this.grantUriPermission(packageName, path, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            startActivity(chooser);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    private void getCSV(int sheet_id) {
        Call<ResponseBody> call = jsonPlaceHolderApi.getExamSheet(sheet_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);

                    Log.d("jsonOb", jsonObject.toString());


                    int student_id = (int) jsonObject.get("student_id");
                    String student_name = (String) jsonObject.get("student_name");
                    String score = String.valueOf(jsonObject.get("score"));
                    JSONArray answers = (JSONArray) jsonObject.get("results");
                    Log.d("jsonAns", answers.toString());

                    if (data.length() == 0) {
                        String exam_name = (String) jsonObject.get("exam_name");
                        data.append("Exam name:," + exam_name + "\n");
                        data.append("ID,Last-Middle Name,First Name,");

                        for (int i = 0; i < answers.length(); i++) {
                            JSONObject answer = answers.getJSONObject(i);
                            data.append(String.valueOf(answer.get("question")) + ",");
                        }

                        data.append("Score\n");
                    }

                    data.append(student_id + ",");
                    String[] splitStr = student_name.split(" ", 10);
                    for (int i = 0; i < splitStr.length - 1; i++) {
                        data.append(splitStr[i] + " ");
                    }
                    data.append(","+splitStr[splitStr.length - 1]);

                    for (int i = 0; i < answers.length(); i++) {
                        JSONObject answer = answers.getJSONObject(i);
                        data.append("," + (String) answer.get("answer"));
                    }
                    data.append("," + score + "\n");


                    Log.d("dataTag", data.toString());

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                Intent intent = new Intent(ScannedTaskActivity.this, HomeTeacherActivity.class);
                return true;
            case R.id.menuUser:
                intent = new Intent(ScannedTaskActivity.this, InfomationActivity.class);
                intent.putExtra("user_id",String.valueOf(user_id));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}