package com.example.autograding.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.autograding.objects.GradeDetailDTO;
import com.example.autograding.R;

import java.util.List;

public class GradeDetailAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private Context context;
    private List<GradeDetailDTO> gradeDetails;
    public GradeDetailAdapter(List<GradeDetailDTO> gradeDetails, Context context)
    {
        this.gradeDetails=gradeDetails;
        this.context=context;
    }
    @Override
    public int getCount() {
        return gradeDetails.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = LayoutInflater.from(context);
        View view= layoutInflater.inflate(R.layout.object_grade_detail,container,false);
        TextView tv_maxExamGrade, tv_examName, tv_duration, tv_yourScore,tv_highestScore,tv_avgScore, tv_bar_yourScore;
        ProgressBar progressBarGrade= view.findViewById(R.id.progessBar_grade);
        int yourScore=gradeDetails.get(position).getYourScore();
        int topper=gradeDetails.get(position).getHighestScore();
        int averageScore=gradeDetails.get(position).getAvgScore();
        int maximumGrade=gradeDetails.get(position).getMaximumScore();

        tv_maxExamGrade= view.findViewById(R.id.tv_MaxExamGrade);
        tv_examName= view.findViewById(R.id.tv_examName);
        tv_duration=view.findViewById(R.id.tv_duration);
        tv_yourScore= view.findViewById(R.id.tv_myGrade);
        tv_highestScore= view.findViewById(R.id.barValueTopper);
        tv_avgScore= view.findViewById(R.id.barValueAverage);
        tv_bar_yourScore= view.findViewById(R.id.barValueYou);

        tv_maxExamGrade.setText(maximumGrade+" Marks");
        tv_examName.setText(gradeDetails.get(position).getExamName());
        tv_duration.setText(gradeDetails.get(position).getDuration()+" Mins");
        tv_yourScore.setText(yourScore+"");
        tv_avgScore.setText(averageScore+"");
        tv_highestScore.setText(topper+"");
        tv_bar_yourScore.setText(yourScore+"");
        int gradePercent= yourScore*100/maximumGrade;
        progressBarGrade.setProgress(gradePercent);


        // change height of bar in chart
        LinearLayout barYou= (LinearLayout) view.findViewById(R.id.bar_you);
        LinearLayout barTopper= view.findViewById(R.id.bar_topper);
        LinearLayout barAverage= view.findViewById(R.id.bar_average);
        // height = score+150
        ViewGroup.LayoutParams params1= barYou.getLayoutParams();
        params1.height= yourScore*3-20;
        barYou.setLayoutParams(params1);

        ViewGroup.LayoutParams params2= barTopper.getLayoutParams();
        params2.height= topper*3-20;
        barTopper.setLayoutParams(params2);

        ViewGroup.LayoutParams params3= barAverage.getLayoutParams();
        params3.height= averageScore*3-20;
        barAverage.setLayoutParams(params3);

        container.addView(view,0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}
