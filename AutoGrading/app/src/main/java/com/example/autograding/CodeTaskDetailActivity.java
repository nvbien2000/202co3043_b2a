package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.autograding.adapter.ListViewAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CodeTaskDetailActivity extends AppCompatActivity {

    private ArrayList<HashMap<String,String>> list;
    public static final String FIRST_COLUMN = "First";
    public static final String SECOND_COLUMN = "Second";
    private int id;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    TextView tvNameCourse;
    TextView tvExamName;
    TextView tvStdName;
    TextView tvStdId;
    TextView tvScore;
    TextView tvLocation;
    TextView tvIP;
//    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_task_detail);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Bundle extras = getIntent().getExtras();
        String subject_name = extras.getString("subject_name");
        tvNameCourse = findViewById(R.id.tv_nameCourse);
        tvNameCourse.setText(subject_name);
        tvExamName = findViewById(R.id.tv_examName);
        tvStdName = findViewById(R.id.tv_stdname);
        tvStdId = findViewById(R.id.tv_stdid);
        tvScore = findViewById(R.id.tv_score);
        tvLocation = findViewById(R.id.tv_location);
        tvIP = findViewById(R.id.tv_ip);

        int sheet_id = Integer.parseInt(extras.getString("sheet_id"));
        getSheet(sheet_id);

        ImageButton btn_back= (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        FloatingActionButton home_btn = findViewById(R.id.home_btn);
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CodeTaskDetailActivity.this, HomeTeacherActivity.class);
                startActivity(intent);
                finish();
            }
        });

        BottomAppBar bar = findViewById(R.id.bottomAppBar);
        setSupportActionBar(bar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                Intent intent = new Intent(CodeTaskDetailActivity.this, HomeTeacherActivity.class);
                return true;
            case R.id.menuUser:
                intent = new Intent(CodeTaskDetailActivity.this, InfomationActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getSheet(int sheet_id) {
        list = new ArrayList<HashMap<String,String>>();
        Call<ResponseBody> call = jsonPlaceHolderApi.getCodeExamSheet(sheet_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    int student_id = (int) jsonObject.get("student_id");
                    String student_name = (String) jsonObject.get("student_name");
                    String score = String.valueOf(jsonObject.get("score"));
                    String exam_name = (String) jsonObject.get("exam_name");
                    String location = (String) jsonObject.get("checkin_loc");
                    String ipAddr = (String) jsonObject.get("checkin_ip");
                    String image_link = (String) jsonObject.get("checkin_img");
                    String image_processed_link = new StringBuilder(image_link.replace("\"","")).toString();
                    tvExamName.setText(exam_name);
                    tvStdName.setText("Name: " + student_name);
                    tvStdId.setText("ID: " + student_id);
                    tvScore.setText("Score: " + score);
                    tvLocation.setText("Check in at: " + location);
                    tvIP.setText("With IP: " + ipAddr);
                    ImageView imageView = (ImageView) findViewById(R.id.iv_picture);
                    if (imageView == null) {
                        Toast.makeText(CodeTaskDetailActivity.this, "Image is null", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(CodeTaskDetailActivity.this, "Please wait, Image is processing...", Toast.LENGTH_SHORT).show();
                        Picasso.get()
                                .load(image_processed_link)
                                .fit().centerInside()
                                .into(imageView);
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}