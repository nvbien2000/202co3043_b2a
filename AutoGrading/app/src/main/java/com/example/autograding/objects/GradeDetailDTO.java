package com.example.autograding.objects;

public class GradeDetailDTO {
    private String courseID;
    private String examName;
    private int duration;
    private int yourScore;
    private int maximumScore;
    private int avgScore;
    private int highestScore;

    public GradeDetailDTO(String courseID, String examName, int duration, int yourScore, int maximumScore, int avgScore, int highestScore) {
        this.courseID = courseID;
        this.examName = examName;
        this.duration = duration;
        this.yourScore = yourScore;
        this.maximumScore = maximumScore;
        this.avgScore = avgScore;
        this.highestScore = highestScore;
    }

    public int getDuration() {
        return duration;
    }

    public int getYourScore() {
        return yourScore;
    }

    public int getMaximumScore() {
        return maximumScore;
    }

    public int getAvgScore() {
        return avgScore;
    }

    public int getHighestScore() {
        return highestScore;
    }

    public String getCourseID() {
        return courseID;
    }
    public String getExamName() {
        return examName;
    }



}
