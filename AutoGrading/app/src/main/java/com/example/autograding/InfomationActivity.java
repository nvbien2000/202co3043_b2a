package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.example.autograding.DTO.User;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;
import org.w3c.dom.Text;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InfomationActivity extends AppCompatActivity {

    TextView tv_fullname;
    TextView tv_fullname1;
    TextView tv_faculty;
    TextView tv_typeaccount;
    TextView tv_email;
    TextView tv_class_degreee_label;
    TextView tv_class_degreee;
    boolean isTeacher;
    int user_id;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infomation);


        tv_fullname=findViewById(R.id.tv_fullname);
        tv_fullname1= findViewById(R.id.tv_fullname1);
        tv_faculty= findViewById(R.id.tv_faculty);
        tv_typeaccount= findViewById(R.id.tv_typeaccount);
        tv_email= findViewById(R.id.tv_email);
        tv_class_degreee_label= findViewById(R.id.tv_class_degreee_label);
        tv_class_degreee= findViewById(R.id.tv_class_degreee);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            user_id = Integer.parseInt(extras.getString("user_id"));
        }

        LoadInfo();


        Button logout_btn = findViewById(R.id.logout_btn);
        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InfomationActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        FloatingActionButton home_btn = findViewById(R.id.home_btn);
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (isTeacher == false) {
                    intent = new Intent(InfomationActivity.this, HomeStudentActivity.class);
                    intent.putExtra("user_id",String.valueOf(user_id));
                }
                else {
                    intent = new Intent(InfomationActivity.this, HomeTeacherActivity.class);
                    intent.putExtra("user_id",String.valueOf(user_id));
                }
                startActivity(intent);
                finish();
            }
        });

        BottomAppBar bar = findViewById(R.id.bottomAppBar);
        setSupportActionBar(bar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
//                Intent intent = new Intent(InfomationActivity.this, HomeStudentActivity.class);
                return true;
            case R.id.menuUser:
//                intent = new Intent(InfomationActivity.this, InfomationActivity.class);
//                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void LoadInfo()
    {
        Call<ResponseBody> call = jsonPlaceHolderApi.get_info(User.getInstance().getUserID());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject= new JSONObject(jsonData);
                    tv_fullname.setText(jsonObject.getString("full_name"));
                    tv_fullname1.setText(jsonObject.getString("full_name"));
                    tv_faculty.setText(jsonObject.getString("faculty"));
                    tv_email.setText(jsonObject.getString("email"));
                    if (jsonObject.getBoolean("is_teacher")==false)
                    {
                        tv_typeaccount.setText("Student");
                        tv_class_degreee_label.setText("Class:");
                        tv_class_degreee.setText(jsonObject.getString("st_class"));
                        isTeacher = false;
                    }
                    else
                    {
                        tv_typeaccount.setText("Teacher");
                        tv_class_degreee_label.setText("Degree:");
                        tv_class_degreee.setText(jsonObject.getString("degree"));
                        isTeacher = true;
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

        });
    }
}