package com.example.autograding;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.autograding.adapter.ExamAdapterCopyBien;
import com.example.autograding.adapter.ListViewAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateExamActivity extends AppCompatActivity {
    int user_id;
    private ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String, String>>();
    private JSONArray testcase = new JSONArray();
    JsonPlaceHolderApi jsonPlaceHolderApi;
    public static final String FIRST_COLUMN = "First";
    public static final String SECOND_COLUMN = "Second";

    EditText start_time;
    EditText end_time;
    EditText start_date;
    EditText end_date;
    EditText exam_name;
    TextView name_course;
    EditText question;
    String start;
    String end;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_exam);
        Bundle extras = getIntent().getExtras();
        user_id = Integer.parseInt(extras.getString("user_id"));
        int course_id = Integer.parseInt(extras.getString("course_id"));
        String subject_name = extras.getString("subject_name");

        name_course = findViewById(R.id.tv_nameCourse);
        name_course.setText(subject_name);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        ListView listView = (ListView) findViewById(R.id.listView);
        ListViewAdapter adapter = new ListViewAdapter(this, list);
        listView.setAdapter(adapter);

        start_time = findViewById(R.id.etStartTime);
        end_time = findViewById(R.id.etEndTime);
        start_date = findViewById(R.id.etStartDate);
        end_date = findViewById(R.id.etEndDate);

        start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(CreateExamActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        start = String.format("%02d:%02d", hourOfDay, minute);
                        start_time.setText(start);
                    }
                }, 0, 0, true);
                timePickerDialog.show();
            }
        });

        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateExamActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        start_date.setText(String.format("%02d/%02d/%04d", dayOfMonth, month+1, year));
                        start = start + " " + start_date.getText();
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(CreateExamActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        end = String.format("%02d:%02d", hourOfDay, minute);
                        end_time.setText(end);
                    }
                }, 0, 0, true);
                timePickerDialog.show();
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateExamActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        end_date.setText(String.format("%02d/%02d/%04d", dayOfMonth, month+1, year));
                        end = end + " " + end_date.getText();
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });



        Button finish_btn = (Button) findViewById(R.id.button);
        finish_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addExam(course_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(CreateExamActivity.this, TeacherExamsActivity_v2.class);
                intent.putExtra("course_id",String.valueOf(course_id));
                intent.putExtra("subject_name", subject_name);
                intent.putExtra("user_id",String.valueOf(user_id));
                startActivity(intent);
                finish();
            }
        });

        Button add_btn = findViewById(R.id.btn_add_testcase);
        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateExamActivity.this);
                final View editLog = getLayoutInflater().inflate(R.layout.add_dialog, null);
                builder.setView(editLog);
                builder.setTitle("Add testcase");
                builder.setNegativeButton("Cancel", (dialog, which) -> {
                });
                builder.setPositiveButton("Add", (dialog, which) -> {
                    EditText input = (EditText) editLog.findViewById(R.id.editText);
                    EditText output = (EditText) editLog.findViewById(R.id.editText1);
                    HashMap<String,String> content = new HashMap<String,String>();
                    content.put(FIRST_COLUMN, input.getText().toString());
                    content.put(SECOND_COLUMN, output.getText().toString());
                    list.add(content);
                    JSONObject tc = new JSONObject();
                    try {
                        tc.put("input",input.getText().toString());
                        tc.put("output", output.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    testcase.put(tc);
                    adapter.notifyDataSetChanged();
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        //back button clicked
        ImageButton btnBack= findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        FloatingActionButton home_btn = findViewById(R.id.home_btn);
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateExamActivity.this, HomeTeacherActivity.class);
                intent.putExtra("user_id",String.valueOf(user_id));
                startActivity(intent);
                finish();
            }
        });

        BottomAppBar bar = findViewById(R.id.bottomAppBar);
        setSupportActionBar(bar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                Intent intent = new Intent(CreateExamActivity.this, HomeTeacherActivity.class);
                return true;
            case R.id.menuUser:
                intent = new Intent(CreateExamActivity.this, InfomationActivity.class);
                intent.putExtra("user_id",String.valueOf(user_id));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addExam(int course_id) throws JSONException {
        JSONObject body = new JSONObject();
        exam_name = findViewById(R.id.etExamName);
        question = findViewById(R.id.exam_question);
        body.put("exam_name", exam_name.getText());
        body.put("datetime_start", start);
        body.put("datetime_end", end);
        body.put("question", question.getText());
        body.put("testcase", testcase);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),body.toString());
        Call<Void> call = jsonPlaceHolderApi.addExam(course_id, requestBody);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(CreateExamActivity.this, "success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(CreateExamActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}