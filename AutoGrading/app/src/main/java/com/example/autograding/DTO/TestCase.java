package com.example.autograding.DTO;

public class TestCase {
    private int examID;
    private String stdin;
    private String stdout;
    private String expected;
    private boolean isSuccess= true;
    private String status="Not Compile !";
    private String timeExecute= "0";
    public TestCase(int examID, String stdin, String stdout,String expected ) {
        this.examID = examID;
        this.stdin = stdin;
        this.stdout = stdout;
        this.expected=expected;
    }

    public boolean getIsSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public void setStdout(String stdout) {
        this.stdout = stdout;
    }

    public String getExpected() {
        return expected;
    }

    public String getTimeExecute() {
        return timeExecute;
    }

    public void setTimeExecute(String timeExecute) {
        this.timeExecute = timeExecute;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getExamID() {
        return examID;
    }

    public String getStdin() {
        return stdin;
    }

    public String getStdout() {
        return stdout;
    }


}
