package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.autograding.DTO.User;
import com.example.autograding.FragmentCodeExam.FragmentCodeExamEditor;
import com.example.autograding.FragmentCodeExam.FragmentCodeExamTestcase;
import com.example.autograding.adapter.ViewPagerAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CodeExamTypeV2 extends AppCompatActivity implements FragmentCodeExamEditor.SendMessage  {

   // TabLayout tabLayout;

    private int exam_id=1;
    private String subject_id="";
    private String examName="";
    TextView tv_examName;
    TextView tv_exam;
    ViewPager viewPager;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_exam_type_v2);
        //tabLayout= findViewById(R.id.tablayoutCodeEditor);
        viewPager= findViewById(R.id.viewPagerCodeExam);
        tv_examName= findViewById(R.id.tv_examName);
        tv_exam= findViewById(R.id.tv_exam);


        exam_id= getIntent().getIntExtra("exam_id",-1);

        User.getInstance().setCurrent_exam_id(exam_id);
        examName=getIntent().getStringExtra("exam_name");
        subject_id=getIntent().getStringExtra("subject_id");

        tv_examName.setText(examName);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        getQuestion();



        ImageButton btnBack= findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        getTab();

    }
    private void getTab()
    {
        final ViewPagerAdapter viewPagerAdapter= new ViewPagerAdapter(getSupportFragmentManager());
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ViewPagerAdapter viewPagerAdapter =new ViewPagerAdapter(getSupportFragmentManager());

                viewPagerAdapter.addFragment(FragmentCodeExamEditor.getInstance(),"Coding Exam");
                viewPagerAdapter.addFragment(FragmentCodeExamTestcase.getInstance(),"Test case");
                viewPager.setAdapter(viewPagerAdapter);
                //tabLayout.setupWithViewPager(viewPager);
            }
        });
    }


    private void getQuestion()
    {
        Call<ResponseBody> call = jsonPlaceHolderApi.get_question(exam_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);

                    tv_exam.setText(jsonObject.get("result").toString());

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    @Override
    public void sendData(String message) throws JSONException, ExecutionException, InterruptedException {

        viewPager.setCurrentItem(1);
        FragmentCodeExamTestcase f = (FragmentCodeExamTestcase) getSupportFragmentManager().getFragments().get(1);
        f.receivedData(message);
    }

}