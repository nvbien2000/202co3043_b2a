package com.example.autograding.FragmentCodeExam;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.autograding.CreateExamActivity;
import com.example.autograding.DTO.JdoodleKey;
import com.example.autograding.DTO.User;
import com.example.autograding.GradeDetailActivity;
import com.example.autograding.R;
import com.example.autograding.ShowScoreExamActivity;
import com.example.autograding.api.JsonPlaceHolderApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import de.markusressel.kodeeditor.library.view.CodeEditorLayout;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import okhttp3.ResponseBody;
public class FragmentCodeExamEditor extends Fragment {

   // private SharedViewModel viewModel;//shared view model pass data between two fragment

    public interface SendMessage {
        void sendData(String message) throws JSONException, ExecutionException, InterruptedException;
    }
    SendMessage SM;

    JsonPlaceHolderApi jsonPlaceHolderApi;
    private TextView terminal;
    private CodeEditorLayout codeEditorLayout;
    private Button btnRunCode;
    private Button btnRunTestCase;
    private String JSONCode;
    private Button btnSubmit;

    public static FragmentCodeExamEditor getInstance()
    {
        FragmentCodeExamEditor fragmentCodeExamEditor= new FragmentCodeExamEditor();
        return  fragmentCodeExamEditor;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            SM = (SendMessage) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_code_exam_editor,container,false);

        terminal= view.findViewById(R.id.terminal);
        btnSubmit= view.findViewById(R.id.btnSubmit);
        terminal.setMovementMethod(new ScrollingMovementMethod());
        codeEditorLayout=view.findViewById(R.id.codeEditorView);
        btnRunCode= view.findViewById(R.id.btnRun);
        btnRunTestCase=view.findViewById(R.id.btnRunTestCase);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);




        btnRunCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    terminal.setText("Executing...");
                    CompilerCode(codeEditorLayout.getText(),"Python","none");
                } catch (JSONException e) {
                    terminal.setText(e.toString());
                    e.printStackTrace();
                }
            }
        });
       return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CompileCode();
                //temp
                Intent grade = new Intent(getActivity(), ShowScoreExamActivity.class);
                grade.putExtra("exam_id",getActivity().getIntent().getIntExtra("exam_id",-1));
                grade.putExtra("exam_name",getActivity().getIntent().getStringExtra("exam_name"));

                int score= getActivity().getIntent().getIntExtra("score",0);
                try {
                    addSheet(score);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(grade);
                getActivity().finish();
            }
        });

        btnRunTestCase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    CompileCode();
            }
        });
    }

    private void addSheet(int score) throws JSONException
    {
        JSONObject body= new JSONObject();
        body.put("student_id", User.getInstance().getUserID());
        body.put("score",score);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),body.toString());
        Call<ResponseBody> call =jsonPlaceHolderApi.add_sheet_code(User.getInstance().getCurrent_exam_id(),requestBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }
    private void CompileCode()
    {
        String languageCode="python3";
        String versionIndex="0";
        String JSON="";
        try {
            JSON= createJson(languageCode,versionIndex,codeEditorLayout.getText(),"\n");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            SM.sendData(JSON);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public String createJson(String languageCode, String versionIndex, String code, String input) throws JSONException {
        JSONObject json= new JSONObject();
        json.put("clientId", JdoodleKey.getInstance().getCLIENT_ID());
        json.put("clientSecret",JdoodleKey.getInstance().getCLIENT_SECRET());
        json.put("script",code);
        json.put("language",languageCode);
        if (input.length()!=0) {
            json.put("stdin", input);
        }
        json.put("versionIndex",versionIndex);
        return json.toString();
    }
    public void CompilerCode(String code,String language,String input) throws JSONException {
        String languageCode="";
        String versionIndex="-1";
        if (language=="C++")
        {
            languageCode="cpp";
            versionIndex="0";

        }
        else if (language=="Python")
        {
            languageCode="python3";
            versionIndex="0";
        }
        String json= createJson(languageCode,versionIndex,code,input);
        JSONCode=json;
        new AsyncPostRequest().execute(json);

    }

    public String FormatOutput(String output) throws JSONException {
        JSONObject json = new JSONObject(output);
        String result="";
        result+=json.getString("output");
        result+="\n";
        result+="Time executed: "+json.getString("cpuTime")+" s";
        return  result;
    }

    private class AsyncPostRequest extends AsyncTask<String,Void,Void>
    {

        @Override
        protected Void doInBackground(String... strings) {
            try {
                String json = strings[0];
                URL url = new URL("https://api.jdoodle.com/v1/execute");
                Log.i("JSON",json);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(json.getBytes());
                outputStream.close();
                outputStream.flush();
                Log.i("STATUS",String.valueOf(connection.getResponseCode()));
                Log.i("MSG",connection.getResponseMessage());
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    terminal.setText(terminal.getText() + "\nError !");
                }
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String result= bufferedReader.readLine();
                Log.i("Respone:",result);
                result=FormatOutput(result);

                terminal.setText(result);

            } catch (MalformedURLException e) {
                terminal.setText(e.toString());
                e.printStackTrace();
            } catch (IOException e) {
                terminal.setText(e.toString());
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }



}
