package com.example.autograding.FragmentTeacherExams;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.autograding.CreateExamActivity;
import com.example.autograding.R;
import com.example.autograding.ScannedTaskActivity;
import com.example.autograding.TeacherExamsActivity_v2;
import com.example.autograding.adapter.ExamAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FragmentExamsText extends Fragment {
    int user_id;
    private ArrayList<HashMap<String,String>> list;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    public static final String EXAM_DATE = "1";
    public static final String EXAM_DURATION = "2";
    public static final String EXAM_NAME ="3";
    public static final String EXAM_ID= "4";
    FloatingActionButton btn_addExam;
    String course_name;
    String start;
    Calendar calendar;
    String start_time;
    String start_date;

    public  static FragmentExamsText getInstance()
    {
        FragmentExamsText fragmentExamsText= new FragmentExamsText();
        return  fragmentExamsText;
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_teacher_exams_text,container,false);
        btn_addExam = view.findViewById(R.id.btn_add_exam);

        TeacherExamsActivity_v2 activity = (TeacherExamsActivity_v2) getActivity();
        int course_id = activity.getCourseId();
        course_name = activity.getCourseName();
        user_id = activity.getUserId();
        //Load list of exams code

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        getExams(view, course_id);

        FloatingActionButton add_exam_btn = view.findViewById(R.id.btn_add_exam);
        add_exam_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final View editLog = getLayoutInflater().inflate(R.layout.add_exam_dialog, null);
                builder.setView(editLog);
                builder.setTitle("Add Exam");
                builder.setNegativeButton("Cancel", (dialog, which) -> {
                });
                builder.setPositiveButton("Add", (dialog, which) -> {
                    EditText name = (EditText) editLog.findViewById(R.id.etName);
                    EditText duration = (EditText) editLog.findViewById(R.id.etDuration);
                    EditText dateStart = editLog.findViewById(R.id.etDate);
                    EditText timeStart = editLog.findViewById(R.id.etTime);

                    start = timeStart.getText().toString() + ' ' + dateStart.getText().toString();

                    JSONObject body = new JSONObject();
                    try {
                        body.put("exam_name",name.getText().toString());
                        body.put("duration", duration.getText().toString());
                        body.put("datetime_start", start);
                        body.put("num_of_ques", String.valueOf(20));
                        addExam(body, course_id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        return  view;

    }

    private void addExam(JSONObject body, int course_id) {
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),body.toString());
        Call<ResponseBody> call = jsonPlaceHolderApi.addMtcExam(course_id, requestBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(FragmentExamsText.this).attach(FragmentExamsText.this).commit();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getExams(View view, int course_id) {
        list = new ArrayList<HashMap<String, String>>();
        Call<ResponseBody> call = jsonPlaceHolderApi.getMultichoiceExam(course_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray exams = (JSONArray) jsonObject.get("results");
                    for (int i = 0; i < exams.length(); i++) {
                        JSONObject exam = exams.getJSONObject(i);
                        HashMap<String,String> map = new HashMap<String,String>();
                        map.put(EXAM_DATE, (String) exam.get("exam_date"));
                        map.put(EXAM_DURATION, String.valueOf(exam.get("exam_length")));
                        map.put(EXAM_NAME, (String) exam.get("exam_name"));
                        map.put(EXAM_ID, String.valueOf(exam.get("exam_id")));
                        list.add(map);
                    }
                    ListView listView = view.findViewById(R.id.listViewExamText);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(getActivity(),ScannedTaskActivity.class);
                            intent.putExtra("subject_name", course_name);
                            intent.putExtra("course_id", String.valueOf(course_id));
                            intent.putExtra("exam_id", list.get(position).get(EXAM_ID));
                            intent.putExtra("user_id",String.valueOf(user_id));
                            startActivity(intent);
                        }
                    });

                    ExamAdapter adapter = new ExamAdapter(getActivity(), list);
                    listView.setAdapter(adapter);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

}
