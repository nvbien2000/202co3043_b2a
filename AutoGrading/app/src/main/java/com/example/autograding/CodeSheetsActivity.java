package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.autograding.adapter.ScannedTaskAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CodeSheetsActivity extends AppCompatActivity {
    int user_id;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    String subject_name;
    private ArrayList<HashMap<String,String>> list;
    public static final String FIRST_COLUMN = "First";
    public static final String SECOND_COLUMN = "Second";
    public static final String THIRD_COLUMN = "Third";

    TextView tvNameCourse;

    int count0 = 0;
    int count1 = 0;
    int count2 = 0;
    int count3 = 0;

    TextView tvC0, tvC1, tvC2, tvC3;
    LinearLayout bar0, bar1, bar2, bar3;
    LinearLayout.LayoutParams bar_par0, bar_par1, bar_par2, bar_par3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_sheets);

        tvNameCourse = findViewById(R.id.tv_nameCourse);

        tvC0 = findViewById(R.id.tv_count0);
        tvC1 = findViewById(R.id.tv_count1);
        tvC2 = findViewById(R.id.tv_count2);
        tvC3 = findViewById(R.id.tv_count3);

        bar0 = findViewById(R.id.bar0);
        bar1 = findViewById(R.id.bar1);
        bar2 = findViewById(R.id.bar2);
        bar3 = findViewById(R.id.bar3);
        bar_par0 = (LinearLayout.LayoutParams) bar0.getLayoutParams();
        bar_par1 = (LinearLayout.LayoutParams) bar1.getLayoutParams();
        bar_par2 = (LinearLayout.LayoutParams) bar2.getLayoutParams();
        bar_par3 = (LinearLayout.LayoutParams) bar3.getLayoutParams();

        Bundle extras = getIntent().getExtras();
        user_id = Integer.parseInt(extras.getString("user_id"));
        subject_name = extras.getString("subject_name");
        tvNameCourse.setText(subject_name);
        int exam_id = Integer.parseInt(extras.getString("exam_id"));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        getSheets(exam_id);

        FloatingActionButton home_btn = findViewById(R.id.home_btn);
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CodeSheetsActivity.this, HomeTeacherActivity.class);
                intent.putExtra("user_id",String.valueOf(user_id));
                startActivity(intent);
                finish();
            }
        });

        ImageButton btn_back= (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        BottomAppBar bar = findViewById(R.id.bottomAppBar);
        setSupportActionBar(bar);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                Intent intent = new Intent(CodeSheetsActivity.this, HomeTeacherActivity.class);
                return true;
            case R.id.menuUser:
                intent = new Intent(CodeSheetsActivity.this, InfomationActivity.class);
                intent.putExtra("user_id",String.valueOf(user_id));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getSheets(int exam_id) {
        list = new ArrayList<HashMap<String, String>>();
        Call<ResponseBody> call = jsonPlaceHolderApi.getCodeSheets(exam_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray sheets = (JSONArray) jsonObject.get("results");
                    for (int i = 0; i < sheets.length(); i++) {
                        JSONObject sheet = sheets.getJSONObject(i);
                        HashMap<String,String> map = new HashMap<String,String>();
                        map.put(FIRST_COLUMN, (String) sheet.get("student_name"));
                        map.put(SECOND_COLUMN, String.valueOf(sheet.get("score")));
                        map.put(THIRD_COLUMN, String.valueOf(sheet.get("sheet_id")));
                        if ((Double) sheet.get("score") < 3.0) {
                            count0 += 1;
                        }
                        else if ((Double) sheet.get("score") < 5.0) {
                            count1 += 1;
                        }
                        else if ((Double) sheet.get("score") < 8.0) {
                            count2 += 1;
                        }
                        else {
                            count3 += 1;
                        }
                        list.add(map);
                    }
                    ListView listView = findViewById(R.id.listView);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(CodeSheetsActivity.this, CodeTaskDetailActivity.class);
                            intent.putExtra("sheet_id", list.get(position).get(THIRD_COLUMN));
                            intent.putExtra("subject_name", subject_name);
                            startActivity(intent);
                        }
                    });

                    ScannedTaskAdapter adapter = new ScannedTaskAdapter(CodeSheetsActivity.this, list);
                    listView.setAdapter(adapter);

                    tvC0.setText(String.valueOf(count0));
                    tvC1.setText(String.valueOf(count1));
                    tvC2.setText(String.valueOf(count2));
                    tvC3.setText(String.valueOf(count3));
                    int sum = count0 + count1 + count2 + count3;
                    if (sum == 0) { sum=1; }
                    int height0 = count0/sum * 300;
                    int height1 = count1/sum * 300;
                    int height2 = count2/sum * 300;
                    int height3 = count3/sum * 300;
                    bar_par0.height = height0; bar0.setLayoutParams(bar_par0);
                    bar_par1.height = height1; bar1.setLayoutParams(bar_par1);
                    bar_par2.height = height2; bar2.setLayoutParams(bar_par2);
                    bar_par3.height = height3; bar3.setLayoutParams(bar_par3);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }
}