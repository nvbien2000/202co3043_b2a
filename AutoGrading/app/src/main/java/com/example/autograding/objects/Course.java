package com.example.autograding.objects;

public class Course {
    private int id;
    private int teacher_id;
    private String subject_id;

    public Course(int id, int teacher_id, String subject_id) {
        this.id = id;
        this.teacher_id = teacher_id;
        this.subject_id = subject_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }
}
