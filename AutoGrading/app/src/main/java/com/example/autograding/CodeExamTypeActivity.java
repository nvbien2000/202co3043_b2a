package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import de.markusressel.kodeeditor.library.view.CodeEditorLayout;
import retrofit2.http.POST;

public class CodeExamTypeActivity extends AppCompatActivity {

    private static String CLIENT_ID="b8c27f1f734bf1cc03bda99c83e704a";
    private static String CLIENT_SECRET="a200af293ac46292a44c67c3ab7c8204d43df5747d3ba03aa8ea46b04e405bb6";
    private TextView terminal;
    private CodeEditorLayout codeEditorLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_exam_type);

        terminal= findViewById(R.id.terminal);
        terminal.setMovementMethod(new ScrollingMovementMethod());
        codeEditorLayout=findViewById(R.id.codeEditorView);
        Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CodeExamTypeActivity.this, CodeExamResultActivity.class);
                startActivity(intent);

            }
        });

        Button btnRunCode= findViewById(R.id.btnRun);
        btnRunCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    terminal.setText("Executing...");
                    CompilerCode(codeEditorLayout.getText(),"Python","2");
                } catch (JSONException e) {
                    terminal.setText(e.toString());
                    e.printStackTrace();
                }
            }
        });


        ImageButton btn_back= (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private String createJson(String languageCode,String versionIndex,String code, String input) throws JSONException {
        JSONObject json= new JSONObject();
        json.put("clientId",CLIENT_ID);
        json.put("clientSecret",CLIENT_SECRET);
        json.put("script",code);
        json.put("language",languageCode);
        if (input.length()!=0) {
            json.put("stdin", input);
        }
        json.put("versionIndex",versionIndex);
        return json.toString();
    }
    private  void PostRequest(String json)
    {

    }
    private void CompilerCode(String code,String language,String input) throws JSONException {
        String languageCode="";
        String versionIndex="-1";
        if (language=="C++")
        {
            languageCode="cpp";
            versionIndex="0";

        }
        else if (language=="Python")
        {
            languageCode="python3";
            versionIndex="0";
        }
        String json= createJson(languageCode,versionIndex,code,input);
        new AsyncPostRequest().execute(json);

    }
    private String FormatOutput(String output) throws JSONException {
        JSONObject json = new JSONObject(output);
        String result="";
        result+=json.getString("output");
        result+="\n";
        result+="Time executed: "+json.getString("cpuTime")+" s";
        return  result;
    }

    private class AsyncPostRequest extends AsyncTask<String,Void,Void>
    {

        @Override
        protected Void doInBackground(String... strings) {
            try {
                String json = strings[0];
                URL url = new URL("https://api.jdoodle.com/v1/execute");
                Log.i("JSON",json);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(json.getBytes());
                outputStream.close();
                outputStream.flush();
                Log.i("STATUS",String.valueOf(connection.getResponseCode()));
                Log.i("MSG",connection.getResponseMessage());
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    terminal.setText(terminal.getText() + "\nError !");
                }
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String result= bufferedReader.readLine();
                Log.i("Respone:",result);
                result=FormatOutput(result);

                terminal.setText(result);

            } catch (MalformedURLException e) {
                terminal.setText(e.toString());
                e.printStackTrace();
            } catch (IOException e) {
                terminal.setText(e.toString());
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
