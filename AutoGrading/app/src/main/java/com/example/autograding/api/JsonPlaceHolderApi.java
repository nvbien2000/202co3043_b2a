package com.example.autograding.api;

import com.example.autograding.objects.MyUser;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface JsonPlaceHolderApi {
    @POST("login")
    Call<ResponseBody> signIn(@Body RequestBody params);

    @GET("home-teacher/{id}")
    Call<ResponseBody> getHomeTeacher(@Path("id") int id);

    @GET("home-student/{id}")
    Call<ResponseBody> getHomeStudent(@Path("id") int id);

    @GET("teacher-exam/multichoice/{id}")
    Call<ResponseBody> getMultichoiceExam(@Path("id") int id);

    @GET("teacher-exam/code/{id}")
    Call<ResponseBody> getCodeExam(@Path("id") int id);

    @GET("student-exam/code/get_upcomming_exam/{user_id}")
    Call<ResponseBody> get_upcomming_exam(@Path("user_id")int user_id);

    @GET("student-exam/code/get_exam/{user_id}")
    Call<ResponseBody> get_course_student(@Path("user_id") int user_id);
    @GET("student-exam/code/get_exam_did/{user_id}")
    Call<ResponseBody> get_score_examcode__did(@Path("user_id") int user_id);

    @GET("find-course-id/{subject_id}")
    Call<ResponseBody> find_course_id(@Path("subject_id") String subject_id);

    @GET("student-exam/code/get_question/{exam_id}")
    Call<ResponseBody> get_question(@Path("exam_id") int exam_id);

    @GET("student-exam/code/get_testcase/{exam_id}")
    Call<ResponseBody> get_testcase(@Path("exam_id") int exam_id);

    @GET("get_info/{user_id}")
    Call<ResponseBody> get_info(@Path("user_id") int user_id);

//    @Multipart
//    @POST("upload/{exam_id}/{key}")
//    Call<String> uploadFile(@Path("exam_id") int exam_id, @Path("key") int key, @Part MultipartBody.Part file);

    @Multipart
    @POST("upload/{exam_id}/{key}")
    Call<ResponseBody> uploadFile(@Path("exam_id") int exam_id, @Path("key") int key, @Part MultipartBody.Part file);

    @POST("teacher-exam/code/{course_id}/add_exam")
    Call<Void> addExam(@Path("course_id") int course_id, @Body RequestBody params);

    @POST("teacher-exam/multichoice/{course_id}/add_exam")
    Call<ResponseBody> addMtcExam(@Path("course_id") int course_id, @Body RequestBody params);

    @POST("student-exam/code/add_sheet_code/{exam_id}")
    Call<ResponseBody> add_sheet_code(@Path("exam_id") int exam_id, @Body RequestBody params);

    @GET("sheets/{exam_id}")
    Call<ResponseBody> getSheets(@Path("exam_id") int exam_id);

    @GET("teacher-exam/multichoice/sheet/{sheet_id}")
    Call<ResponseBody> getExamSheet(@Path("sheet_id") int sheet_id);

    @GET("code_sheets/{exam_id}")
    Call<ResponseBody> getCodeSheets(@Path("exam_id") int exam_id);

    @GET("teacher-exam/code/sheet/{sheet_id}")
    Call<ResponseBody> getCodeExamSheet(@Path("sheet_id") int sheet_id);

    @Multipart
    @POST("checkin/{exam_id}/{student_id}/{location}/{ip}")
    Call<ResponseBody> checkin(@Path("exam_id") int exam_id, @Path("student_id") int student_id, @Path("location") String location, @Path("ip") String ip, @Part MultipartBody.Part file);

}
