package com.example.autograding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.format.Formatter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.autograding.DTO.User;
import com.example.autograding.objects.UpcomingExamDTO;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.w3c.dom.Text;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;


import android.os.ResultReceiver;
import android.os.Handler;
import android.location.Geocoder;
import android.location.Location;
public class CheckInActivity extends AppCompatActivity {

    private FusedLocationProviderClient fusedLocationClient;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2;

    private static final int PERMISSION_CODE = 1000;
    private static final int IMAGE_CAPTURE_CODE = 1001;
    Uri imageUri;

    private LocationAddressResultReceiver addressResultReceiver;
    private TextView currentAddTv;
    private Location currentLocation;
    private LocationCallback locationCallback;

    private TextView tv_examName;
    private TextView tv_subjectID;
    private TextView tv_duration;

    private TextView tv_fullname;
    TextView tv_ip;
    String subject_id;
    String exam_info;
    Button btn_checkin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);

        tv_examName= findViewById(R.id.tv_examName);
        tv_duration= findViewById(R.id.lb_duration);
        tv_subjectID= findViewById(R.id.tv_subjectID);
        tv_fullname= findViewById(R.id.tv_fullname);

        tv_fullname.setText(User.getInstance().getFullName());

        exam_info=   getIntent().getStringExtra("exam_info");
        tv_examName.setText(exam_info);
        int duration=  getIntent().getIntExtra("length",60);
        tv_duration.setText(duration+" min");
        subject_id= getIntent().getStringExtra("subject_id");
        tv_subjectID.setText(subject_id);

//        Button checkin_btn = findViewById(R.id.button);
//        checkin_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(CheckInActivity.this, CodeExamTypeV2 .class);
//                intent.putExtra("subject_id", subject_id);
//                intent.putExtra("exam_name",exam_info);
//                intent.putExtra("exam_id", getIntent().getIntExtra("exam_id",0));
//                intent.putExtra("course_id",getIntent().getIntExtra("course_id",0));
//                startActivity(intent);
//                finish();
//            }
//
//        });

        ImageButton btn_back= (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        FloatingActionButton home_btn = findViewById(R.id.home_btn);
//        home_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(CheckInActivity.this, HomeStudentActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });

//        BottomAppBar bar = findViewById(R.id.bottomAppBar);
//        setSupportActionBar(bar);
//        ImageButton btn_resetIp= (ImageButton) findViewById(R.id.btn_resetIp);
//        btn_resetIp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getIpAddress();
//            }
//        });

        getIpAddress();

        //get location
        addressResultReceiver = new LocationAddressResultReceiver(new Handler());
        currentAddTv = findViewById(R.id.tv_location);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                currentLocation = locationResult.getLocations().get(0);
                getAddress();
            }
        };
        startLocationUpdates();

        btn_checkin = findViewById(R.id.button);
        btn_checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check(v);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                Intent intent = new Intent(CheckInActivity.this, HomeStudentActivity.class);
                return true;
            case R.id.menuUser:
                intent = new Intent(CheckInActivity.this, InfomationActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private  void getIpAddress()
    {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        String ipAddress= Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
        tv_ip = (TextView) findViewById(R.id.tv_ipAddress);
        tv_ip.setText(ipAddress);
    }

    private void getAddress() {
        if (!Geocoder.isPresent()) {

            return;
        }
        Intent intent = new Intent(this, GetAddressIntentService.class);
        intent.putExtra("add_receiver", addressResultReceiver);
        intent.putExtra("add_location", currentLocation);
        startService(intent);
    }


    @SuppressWarnings("MissingPermission")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new
                            String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
        else {
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(2000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull
            int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates();
            }
            else {
                Toast.makeText(this, "Location permission not granted, " + "restart the app if you want the feature", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private class LocationAddressResultReceiver extends ResultReceiver {
        LocationAddressResultReceiver(Handler handler) {
            super(handler);
        }
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            ProgressBar progressBar= findViewById(R.id.processBar);
            if (resultCode == 0) {

                getAddress();
            }

            String currentAdd = resultData.getString("address_result");

            if (resultCode==2)
            {
             progressBar.setVisibility(View.INVISIBLE);
            }
            else
            {
                progressBar.setVisibility(View.VISIBLE);
            }

            showResults(currentAdd);
        }
    }
    private void showResults(String currentAdd) {
        currentAddTv.setText(currentAdd);
        btn_checkin.setClickable(true);
        btn_checkin.setVisibility(View.VISIBLE);
    }
    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }
    @Override
    protected void onPause() {
        super.onPause();
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }

    public void check(View v) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                requestPermissions(permissions, PERMISSION_CODE);
            }
            else {
                OpenCamera();
            }
        }
        else {
            OpenCamera();
        }
    }

    private void OpenCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE,"New Image");
        values.put(MediaStore.Images.Media.DESCRIPTION,"from the camera");
        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Intent i = new Intent(CheckInActivity.this, CheckInResultActivity.class);
            i.setData(imageUri);
            i.putExtra("subject_id", subject_id);
            i.putExtra("exam_name",exam_info);
            i.putExtra("exam_id", getIntent().getIntExtra("exam_id",0));
            i.putExtra("course_id",getIntent().getIntExtra("course_id",0));
            i.putExtra("location", currentAddTv.getText());
            i.putExtra("ip", tv_ip.getText());
            startActivity(i);
        }
    }

}


