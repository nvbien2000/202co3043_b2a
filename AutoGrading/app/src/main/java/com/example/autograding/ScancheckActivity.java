package com.example.autograding;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.autograding.adapter.ListViewAdapter;
import com.example.autograding.adapter.ScannedTaskAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ScancheckActivity extends AppCompatActivity {
    int user_id;
    private ArrayList<HashMap<String,String>> list;
    public static final String FIRST_COLUMN = "First";
    public static final String SECOND_COLUMN = "Second";
    private int id;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    TextView tvNameCourse;
    TextView tvExamName;
    TextView tvStdName;
    TextView tvStdId;
    TextView tvScore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scancheck);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Bundle extras = getIntent().getExtras();
        user_id = Integer.parseInt(extras.getString("user_id"));
        String subject_name = extras.getString("subject_name");
        tvNameCourse = findViewById(R.id.tv_nameCourse);
        tvNameCourse.setText(subject_name);
        tvExamName = findViewById(R.id.tv_examName);
        tvStdName = findViewById(R.id.tv_stdname);
        tvStdId = findViewById(R.id.tv_stdid);
        tvScore = findViewById(R.id.tv_score);
        int sheet_id = Integer.parseInt(extras.getString("sheet_id"));
        getSheet(sheet_id);

        ImageButton btn_back= (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        FloatingActionButton home_btn = findViewById(R.id.home_btn);
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScancheckActivity.this, HomeTeacherActivity.class);
                intent.putExtra("user_id",String.valueOf(user_id));
                startActivity(intent);
                finish();
            }
        });

        BottomAppBar bar = findViewById(R.id.bottomAppBar);
        setSupportActionBar(bar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                Intent intent = new Intent(ScancheckActivity.this, HomeTeacherActivity.class);
                return true;
            case R.id.menuUser:
                intent = new Intent(ScancheckActivity.this, InfomationActivity.class);
                intent.putExtra("user_id",String.valueOf(user_id));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getSheet(int sheet_id) {
        list = new ArrayList<HashMap<String,String>>();
        Call<ResponseBody> call = jsonPlaceHolderApi.getExamSheet(sheet_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    int student_id = (int) jsonObject.get("student_id");
                    String student_name = (String) jsonObject.get("student_name");
                    String score = String.valueOf(jsonObject.get("score"));
                    String exam_name = (String) jsonObject.get("exam_name");
                    tvExamName.setText(exam_name);
                    tvStdName.setText("Name: " + student_name);
                    tvStdId.setText("ID: " + student_id);
                    tvScore.setText("Score: " + score);

                    JSONArray answers = (JSONArray) jsonObject.get("results");
                    for (int i = 0; i < answers.length(); i++) {
                        JSONObject answer = answers.getJSONObject(i);
                        HashMap<String,String> map = new HashMap<String,String>();
                        map.put(FIRST_COLUMN, String.valueOf(answer.get("question")));
                        map.put(SECOND_COLUMN, (String) answer.get("answer"));
                        list.add(map);
                    }
                    ListView listView = findViewById(R.id.listView);
                    ListViewAdapter adapter = new ListViewAdapter(ScancheckActivity.this, list);
                    listView.setAdapter(adapter);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}