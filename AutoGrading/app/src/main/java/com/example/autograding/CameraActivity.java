package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.cameraview.AspectRatio;
import com.google.android.cameraview.CameraView;


public class CameraActivity extends AppCompatActivity {
//    private CameraView mCameraView;
//    private static final String TAG = "CameraActivity";
//    private static final int REQUEST_CAMERA_PERMISSION = 1;
//    private Handler mBackgroundHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

//        ImageButton check_btn = (ImageButton) findViewById(R.id.check_btn);
//        check_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Intent intent = new Intent(CameraActivity.this, CodeExamTypeActivity.class);
////                startActivity(intent);
//
//            }
//        });
//
//        ImageButton cancel_btn = (ImageButton) findViewById(R.id.cancel_btn);
//        cancel_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                mCameraView.stop();
//                Intent intent = new Intent(CameraActivity.this, CheckInActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        mCameraView = findViewById(R.id.camera);
//        mCameraView.setAspectRatio(AspectRatio.of(4,3));
//
//        if (mCameraView != null) {
//            mCameraView.addCallback(mCallback);
//        }
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        mCameraView.stop();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
//                == PackageManager.PERMISSION_GRANTED) {
//            mCameraView.start();}
//        else {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
//                    REQUEST_CAMERA_PERMISSION);
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (mBackgroundHandler != null) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
//                mBackgroundHandler.getLooper().quitSafely();
//            } else {
//                mBackgroundHandler.getLooper().quit();
//            }
//            mBackgroundHandler = null;
//        }
//    }
//
//    private Handler getBackgroundHandler() {
//        if (mBackgroundHandler == null) {
//            HandlerThread thread = new HandlerThread("background");
//            thread.start();
//            mBackgroundHandler = new Handler(thread.getLooper());
//        }
//        return mBackgroundHandler;
//    }
//
//    private CameraView.Callback mCallback
//            = new CameraView.Callback() {
//
//        @Override
//        public void onCameraOpened(CameraView cameraView) {
//            Log.d(TAG, "onCameraOpened");
//        }
//
//        @Override
//        public void onCameraClosed(CameraView cameraView) {
//            Log.d(TAG, "onCameraClosed");
//        }
//
//        @Override
//        public void onPictureTaken(CameraView cameraView, final byte[] data) {
//            Log.d(TAG, "onPictureTaken " + data.length);
//            getBackgroundHandler().post(new Runnable() {
//                @Override
//                public void run() {
//                }
//            });
//        }
//
//        @Override
//        public void onPreviewReady() {
//            Log.d(TAG, "onPreviewReady");
//        }
//    };

}