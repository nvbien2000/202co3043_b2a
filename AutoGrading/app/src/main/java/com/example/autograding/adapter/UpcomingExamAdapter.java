package com.example.autograding.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.autograding.CheckInActivity;
import com.example.autograding.R;
import com.example.autograding.objects.UpcomingExamDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UpcomingExamAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private Context context;
    private List<UpcomingExamDTO> upcomingExams= new ArrayList<>();
    public UpcomingExamAdapter(List<UpcomingExamDTO> upcomingExams, Context context)
    {
        this.upcomingExams=upcomingExams;
        this.context=context;
    }
    @Override
    public int getCount() {
        return upcomingExams.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater  = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view       = layoutInflater.inflate(R.layout.object_upcomming_exam,container,false);



        TextView tv_myDay, tv_myHour, tv_myMinute, tv_nameExam, tv_examDate;
        ProgressBar progressBarDay      = view.findViewById(R.id.progessBar_day);
        ProgressBar progressBarHour     = view.findViewById(R.id.progessBar_hour);
        ProgressBar progressBarMinute   = view.findViewById(R.id.progessBar_minute);
        int day     = upcomingExams.get(position).getMyDay();
        int hour    = upcomingExams.get(position).getMyHour();
        int minute  = upcomingExams.get(position).getMyMinute();

        tv_myDay    = view.findViewById(R.id.tv_myDay);
        tv_myHour   = view.findViewById(R.id.tv_myHour);
        tv_myMinute = view.findViewById(R.id.tv_myMinute);
        tv_nameExam = view.findViewById(R.id.tv_nameExam);
        tv_examDate = view.findViewById(R.id.tv_examDate);
        String name="Your "+upcomingExams.get(position).getNameExam()+" is scheduled on";
        tv_nameExam.setText(name);
        tv_examDate.setText(upcomingExams.get(position).getExamDate());
        tv_myDay.setText(day+"");
        tv_myHour.setText(hour+"");
        tv_myMinute.setText(minute+"");

        progressBarDay.setProgress(day);
        progressBarHour.setProgress(hour);
        progressBarMinute.setProgress(minute);

        container.addView(view,0);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CheckInActivity.class);
                intent.putExtra("exam_info", upcomingExams.get(position).getNameExam());
                intent.putExtra("length",upcomingExams.get(position).getDuration());
                intent.putExtra("subject_id",upcomingExams.get(position).getSubject_id());
                intent.putExtra("course_id", upcomingExams.get(position).getCourse_id());
                intent.putExtra("exam_id",upcomingExams.get(position).getExam_id());
                context.startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}
