package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.example.autograding.DTO.User;
import com.example.autograding.adapter.GradeDetailAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.example.autograding.objects.GradeDetailDTO;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GradeDetailActivity extends AppCompatActivity {
    ViewPager viewPager;
    GradeDetailAdapter gradeDetailAdapter;
    List<GradeDetailDTO> gradeDetails = new ArrayList<>();
    DotsIndicator dotsIndicator;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    private String subject_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade_detail);

        subject_id=getIntent().getStringExtra("subject_id");
        dotsIndicator= findViewById(R.id.dots_indicator);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        getAllScore();









        ImageButton btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                finish();
            }
        });




        // grade detail adapter
//        gradeDetails= new ArrayList<>();
//        gradeDetails.add(new GradeDetailDTO("1234","DSA",120,90,100,65,100));
//        gradeDetails.add(new GradeDetailDTO("12346","DSA2",50,65,100,65,100));
//        gradeDetails.add(new GradeDetailDTO("13123","DSA3",90 ,95,100,55,100));
//        gradeDetails.add(new GradeDetailDTO("1212","DSA4",90 ,5,100,95,100));
//        gradeDetails.add(new GradeDetailDTO("1132","DSA5",12 ,40,100,55,60));
//        gradeDetailAdapter = new GradeDetailAdapter(gradeDetails,this);
        viewPager= findViewById(R.id.viewPagerGradeDetail);
//        viewPager.setAdapter(gradeDetailAdapter);
//        dotsIndicator.setViewPager(viewPager);
    }

    private void getAllScore()
    {
        gradeDetails.clear();
        Call<ResponseBody> call = jsonPlaceHolderApi.get_score_examcode__did(User.getInstance().getUserID());
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray scores = (JSONArray) jsonObject.get("result");
                    for (int i=0;i<scores.length();i++)
                    {
                        JSONObject course = scores.getJSONObject(i);
                        if (course.getString("subject_id").equals(subject_id))
                        {
                            gradeDetails.add(new GradeDetailDTO("1",course.getString("exam_name"),course.getInt("length"),course.getInt("score:"),100,course.getInt("avg"),course.getInt("topper")));
                        }
                    }

                    gradeDetailAdapter = new GradeDetailAdapter(gradeDetails,GradeDetailActivity.this);
                    viewPager.setAdapter(gradeDetailAdapter);
                    dotsIndicator.setViewPager(viewPager);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}