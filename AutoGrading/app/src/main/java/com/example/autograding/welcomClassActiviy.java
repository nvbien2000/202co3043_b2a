package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class welcomClassActiviy extends AppCompatActivity {

    private String subject_name;
    private String subject_id;
    TextView tv_className;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcom_class);

        tv_className= findViewById(R.id.tv_className);
        subject_name= getIntent().getStringExtra("subject_name");
        subject_id= getIntent().getStringExtra("subject_id");
        tv_className.setText(subject_name);

        Button btn_dashboard= (Button) findViewById(R.id.btn_dashboards);
        btn_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getApplicationContext(),GradeDetailActivity.class);
                intent.putExtra("subject_id",subject_id);
                startActivity(intent);
            }
        });

        ImageButton btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}