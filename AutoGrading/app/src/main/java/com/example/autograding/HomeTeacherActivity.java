package com.example.autograding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.autograding.adapter.ExamAdapterCopyBien;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HomeTeacherActivity extends AppCompatActivity {
    private ArrayList<HashMap<String, String>> list;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    public static final String SUBJECT_TEACHER = "1";
    public static final String SUBJECT_ID = "2";
    public static final String SUBJECT_NAME ="3";
    public static final String COURSE_ID ="4";
    TextView tv_name;
    int user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_teacher);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        tv_name=findViewById(R.id.tv_name);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            user_id = Integer.parseInt(extras.getString("user_id"));
            getCourse(user_id);
            getName(user_id);
        }

//        FloatingActionButton home_btn = findViewById(R.id.home_btn);
//        home_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(HomeTeacherActivity.this, HomeTeacherActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });

        BottomAppBar bar = findViewById(R.id.bottomAppBar);
        setSupportActionBar(bar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                Intent intent = new Intent(HomeTeacherActivity.this, HomeTeacherActivity.class);
                return true;
            case R.id.menuUser:
                intent = new Intent(HomeTeacherActivity.this, InfomationActivity.class);
                intent.putExtra("user_id",String.valueOf(user_id));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getName(int user_id)
    {
        Call<ResponseBody> call = jsonPlaceHolderApi.get_info(user_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject= new JSONObject(jsonData);
                    tv_name.setText("Hi, "+jsonObject.getString("name"));
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getCourse(int user_id) {
        list = new ArrayList<HashMap<String, String>>();
        Call<ResponseBody> call = jsonPlaceHolderApi.getHomeTeacher(user_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray courses = (JSONArray) jsonObject.get("results");
                    String teacher_name = (String) jsonObject.get("teacher_name");
                    for (int i = 0; i < courses.length(); i++) {
                        JSONObject course = courses.getJSONObject(i);
                        HashMap<String,String> map = new HashMap<String,String>();
                        map.put(SUBJECT_TEACHER, teacher_name);
                        map.put(SUBJECT_NAME, (String) course.get("subject_name"));
                        map.put(SUBJECT_ID, (String) course.get("subject_id"));
                        map.put(COURSE_ID, String.valueOf(course.get("course_id")));
                        list.add(map);
                    }
                    ListView listView = findViewById(R.id.listView);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(HomeTeacherActivity.this,TeacherExamsActivity_v2.class);
                            HashMap<String,String> course = list.get(position);
                            intent.putExtra("course_id", course.get(COURSE_ID));
                            intent.putExtra("subject_name", course.get(SUBJECT_NAME));
                            intent.putExtra("user_id",String.valueOf(user_id));
                            startActivity(intent);
                        }
                    });

                    ExamAdapterCopyBien adapter = new ExamAdapterCopyBien(HomeTeacherActivity.this, list);
                    listView.setAdapter(adapter);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }
}