package com.example.autograding.utils;

public interface IUploadCallbacks {
    void onProgressUpdate(int percent);
}
