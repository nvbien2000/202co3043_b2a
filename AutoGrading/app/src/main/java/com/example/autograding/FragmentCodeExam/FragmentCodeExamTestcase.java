package com.example.autograding.FragmentCodeExam;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.autograding.DTO.TestCase;
import com.example.autograding.DTO.User;
import com.example.autograding.R;
import com.example.autograding.adapter.ViewPagerAdapter;
import com.example.autograding.api.JsonPlaceHolderApi;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FragmentCodeExamTestcase extends Fragment {
    List<TestCase> testcases = new ArrayList<>();
    private String JSONcode="";
    private int score=0;
    private int MaxPoint=100;
    public int exam_id;
    JsonPlaceHolderApi jsonPlaceHolderApi;
    public void setMaxPoint(int maxPoint) {
        MaxPoint = maxPoint;
    }

    public int getScore() {
        return score;
    }

    public static FragmentCodeExamTestcase getInstance()
    {
        FragmentCodeExamTestcase fragmentCodeExamTestcase= new FragmentCodeExamTestcase();
        return  fragmentCodeExamTestcase;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    private void getTestCases()
    {

        Call<ResponseBody> call = jsonPlaceHolderApi.get_testcase(User.getInstance().getCurrent_exam_id());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    JSONArray object= (JSONArray) jsonObject.get("result");
                    for (int i=0;i<object.length();i++)
                    {
                        JSONObject course = object.getJSONObject(i);
                        TestCase t= new TestCase(exam_id,course.getString("input"),"",course.getString("output"));
                        testcases.add(t);

                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                getTab();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ProgressBar progressBar;
    private List<String> resultOutput ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_code_exam_testcase,container,false);
        viewPager=view.findViewById(R.id.viewpagerCaseDetail);
        tabLayout= view.findViewById(R.id.nameTestCase);
        progressBar=view.findViewById(R.id.processBar);
        resultOutput= new ArrayList<>();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://autograding-api-b2a.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        getTestCases();

        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);





    }
    public void receivedData(String jsoncde) throws JSONException, ExecutionException, InterruptedException {
        JSONcode=jsoncde;
        RunTestCases();

    }
    private  void getTab()
    {
        final ViewPagerAdapter viewPagerAdapter= new ViewPagerAdapter(getFragmentManager());
        new Handler().post(new Runnable() {
            @Override
            public void run() {

                ViewPagerAdapter viewPagerAdapter =new ViewPagerAdapter(getFragmentManager());

                viewPagerAdapter.Clear();

                for (int i=0; i<testcases.size();i++)
                {
                    viewPagerAdapter.addFragment( new FragmentTestCaseDetail(testcases.get(i).getStatus(),testcases.get(i).getStdin(),testcases.get(i).getStdout(),testcases.get(i).getExpected(),testcases.get(i).getTimeExecute(),testcases.get(i).getIsSuccess()),"Test case "+(i+1));
                }
                viewPager.setAdapter(viewPagerAdapter);
                tabLayout.setupWithViewPager(viewPager);
            };
        });
    }
    public void RunTestCases() throws JSONException, ExecutionException, InterruptedException {
        progressBar.setMax(testcases.size());
        progressBar.setProgress(0);
        resultOutput.clear();
        for (int i=0;i<testcases.size();i++)
        {
            TestCase item= testcases.get(i);
            JSONObject js= new JSONObject(JSONcode);
            js.put("stdin",item.getStdin());
            new AsyncPostRequest().execute(js.toString()).get();
        }
        score=caculateScore();

        //Toast.makeText(getActivity().getApplicationContext(),score+"",Toast.LENGTH_LONG).show();
        getActivity().getIntent().putExtra("score",score);
        getTab();

    }

    private int caculateScore() throws JSONException {
        int score=0;

        for (int i=0;i<testcases.size();i++)
        {
            JSONObject stdout= new JSONObject(resultOutput.get(i));

            testcases.get(i).setStatus("Compiled !");
            testcases.get(i).setTimeExecute(stdout.getString("cpuTime"));
            String output= stdout.getString("output");
            testcases.get(i).setStdout(output);
            if (output.equals(testcases.get(i).getExpected()))
            {
                testcases.get(i).setSuccess(true);
                score+=1;
            }
            else
            {
                testcases.get(i).setSuccess(false);
            }
        }

        int t= testcases.size();
        if (t==0)
            t=1;
        return (int)score*MaxPoint/t;
    }

    public String FormatOutput(String output) throws JSONException {
        JSONObject json = new JSONObject(output);
        String result="";
        result+=json.getString("output");
        result+="\n";
        result+="Time executed: "+json.getString("cpuTime")+" s";
        return  result;
    }

    private class AsyncPostRequest extends AsyncTask<String, Void, Void>
    {

        @Override
        protected Void doInBackground(String... strings) {

            try {
                String json = strings[0];
                URL url = new URL("https://api.jdoodle.com/v1/execute");
                Log.i("JSON",json);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(json.getBytes());
                outputStream.close();
                outputStream.flush();
                    Log.i("STATUS",String.valueOf(connection.getResponseCode()));

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {

                }
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String result= bufferedReader.readLine();
                resultOutput.add(result);




            } catch (MalformedURLException e) {

                e.printStackTrace();
            } catch (IOException e) {

                e.printStackTrace();
            }
            return null;
        }


    }
    
}
