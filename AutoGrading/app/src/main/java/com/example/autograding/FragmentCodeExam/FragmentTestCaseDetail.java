package com.example.autograding.FragmentCodeExam;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.autograding.R;

public class FragmentTestCaseDetail extends Fragment {
    private String msg;
    private String stdin;
    private String stdout;
    private String expected;
    private String timeExecute;
    private boolean issuccess;

    public FragmentTestCaseDetail(String msg, String stdin, String stdout, String expected, String timeExecute, boolean isSuccess) {
        this.msg = msg;
        this.stdin = stdin;
        this.stdout = stdout;
        this.expected = expected;
        this.timeExecute = timeExecute;
        this.issuccess=isSuccess;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_test_case_detail,container,false);

        TextView compilermsg= view.findViewById(R.id.tv_compilerMessage);
        TextView tvstdin= view.findViewById(R.id.tv_stdin);
        TextView tvexpected= view.findViewById(R.id.tv_expected_output);
        TextView yourOutput= view.findViewById(R.id.tv_your_output);
        TextView tvtimeExecute= view.findViewById(R.id.tv_time_execute);
        LinearLayout linearLayout =view.findViewById(R.id.layoutTestCase);
        if (issuccess==false)
        {
            linearLayout.setBackgroundColor(Color.parseColor("#FABBBB"));
        }
        compilermsg.setText(msg);
        tvstdin.setText(stdin);
        tvexpected.setText(expected);
        yourOutput.setText(stdout);
        tvtimeExecute.setText(timeExecute+" s");
        return view;
    }
}
