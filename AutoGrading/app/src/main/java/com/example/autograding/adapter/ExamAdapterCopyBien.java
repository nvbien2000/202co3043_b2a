package com.example.autograding.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.autograding.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ExamAdapterCopyBien extends BaseAdapter {
    public ArrayList<HashMap<String, String>> list;

    public static final String SUBJECT_TEACHER = "1";
    public static final String SUBJECT_ID = "2";
    public static final String SUBJECT_NAME ="3";

    Activity activity;
    public ExamAdapterCopyBien(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        TextView tvSubjectName;
        TextView tvTeacherName;
        TextView tvSubjectID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ExamAdapterCopyBien.ViewHolder holder;

        LayoutInflater inflater = activity.getLayoutInflater();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.object_subject, null);
            holder = new ExamAdapterCopyBien.ViewHolder();
            holder.tvSubjectID = (TextView) convertView.findViewById(R.id.tv_subjectID);
            holder.tvSubjectName = (TextView) convertView.findViewById(R.id.tb_subjectName);
            holder.tvTeacherName= (TextView) convertView.findViewById(R.id.tv_teacher);
            convertView.setTag(holder);
        } else {
            holder = (ExamAdapterCopyBien.ViewHolder) convertView.getTag();
        }

        HashMap<String, String> map = list.get(position);
        holder.tvTeacherName.setText(map.get(SUBJECT_TEACHER));
        holder.tvSubjectID.setText(map.get(SUBJECT_ID));
        holder.tvSubjectName.setText(map.get(SUBJECT_NAME));

        return convertView;
    }
}

