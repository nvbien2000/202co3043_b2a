import time

from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.floatlayout import FloatLayout
from kivy.clock import Clock
from random import sample
from copy import deepcopy
# from sudoku import Sudoku


class SudokuGame(FloatLayout):
    def generateBoard(self):
        # n: the dimension of the interior boxes, the board is n^2 * n^2
        n=3
        side = n*n
        nums = sample(range(1, side + 1), side)  # random numbers
        board_result = [[nums[(n * (r % n) + r // n + c) % side]
                            for c in range(side)] for r in range(side)]
        board_removal = deepcopy(board_result)
        # create flag table. if removal value flag will 1
        numbers_empty = side*side//2
        for i in sample(range(side*side), numbers_empty):
            board_removal[i//side][i % side] = 0
        return board_result, board_removal


    # Initialize the grid of text inputs
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.cells  = [[], [], [], [], [], [], [], [], []]
        self.check_messages = []
        self.wrong_messages = []

        grid = self.ids["grid"]
        for row in range(9):
            for col in range(9):
                text_input = SudokuCell()
                grid.add_widget(text_input)
                self.cells[row].append(text_input)

        # Testcase: AI Escargot - the most diffucult Sudoku
        # self.cells[0][0].text = "8"
        # self.cells[1][2].text = "3"
        # self.cells[1][3].text = "6"
        # self.cells[2][1].text = "7"
        # self.cells[2][4].text = "9"
        # self.cells[2][6].text = "2"
        # self.cells[3][1].text = "5"
        # self.cells[3][5].text = "7"
        # self.cells[4][4].text = "4"
        # self.cells[4][5].text = "5"
        # self.cells[4][6].text = "7"
        # self.cells[5][3].text = "1"
        # self.cells[5][7].text = "3"
        # self.cells[6][2].text = "1"
        # self.cells[6][7].text = "6"
        # self.cells[6][8].text = "8"
        # self.cells[7][2].text = "8"
        # self.cells[7][3].text = "5"
        # self.cells[7][7].text = "1"
        # self.cells[8][1].text = "9"
        # self.cells[8][6].text = "4"

        # # Read all the sudoku values (text -> number)
        # values = [[self.get_value(row, col) for col in range(9)] 
        #                                     for row in range(9)]
        
        # # Try to solve the Sudoku
        # result = Sudoku(values)
        # result.solve()
        # for row in range(9):
        #     for col in range(9):
        #         self.result[row].append(result.get_value(row, col))

        self.new_game()
        

    def new_game(self):
        self.result = [[], [], [], [], [], [], [], [], []]

        for cell in self.ids["grid"].children:
            cell.text = ""

        self.board_result, self.board_removal = self.generateBoard()
        for row in range(9):
            for col in range(9):
                self.result[row].append(self.board_result[row][col])

        for row in range(9):
            for col in range(9):
                if self.board_removal[row][col] != 0:
                    self.cells[row][col].text = str(self.board_removal[row][col])


    def get_value(self, row, col):
        number = self.cells[row][col].text
        return int(number) if len(number) > 0 else 0


    # Solve the sudoku
    def solve(self):
        for row in range(9):
            for col in range(9):
                self.cells[row][col].text = str(self.result[row][col])
    
    # Check the inputs from user
    def check(self):
        values = [[self.get_value(row, col) for col in range(9)] 
                                            for row in range(9)]

        is_wrong = False
        for row in range(9):
            for col in range(9):
                if values[row][col] != 0 and values[row][col] != self.result[row][col]:
                    is_wrong = True
                    self.cells[row][col].text = ""

        if is_wrong == True:
            wrong_message = WrongMessage()
            self.wrong_messages.append(wrong_message)
            self.add_widget(wrong_message)
            Clock.schedule_once(self.remove_wrong_message, 2)
        else:
            check_message = CheckMessage()
            self.check_messages.append(check_message)
            self.add_widget(check_message)
            Clock.schedule_once(self.remove_check_message, 2)


    def remove_check_message(self, dt):
        check_message = self.check_messages.pop()
        self.remove_widget(check_message)


    def remove_wrong_message(self, dt):
        wrong_message = self.wrong_messages.pop()
        self.remove_widget(wrong_message)


    # Clear sudoku (Remove all the numbers)
    def clear(self):
        for cell in self.ids["grid"].children:
            cell.text = ""
        # self.cells[0][0].text = "8"
        # self.cells[1][2].text = "3"
        # self.cells[1][3].text = "6"
        # self.cells[2][1].text = "7"
        # self.cells[2][4].text = "9"
        # self.cells[2][6].text = "2"
        # self.cells[3][1].text = "5"
        # self.cells[3][5].text = "7"
        # self.cells[4][4].text = "4"
        # self.cells[4][5].text = "5"
        # self.cells[4][6].text = "7"
        # self.cells[5][3].text = "1"
        # self.cells[5][7].text = "3"
        # self.cells[6][2].text = "1"
        # self.cells[6][7].text = "6"
        # self.cells[6][8].text = "8"
        # self.cells[7][2].text = "8"
        # self.cells[7][3].text = "5"
        # self.cells[7][7].text = "1"
        # self.cells[8][1].text = "9"
        # self.cells[8][6].text = "4"
        for row in range(9):
            for col in range(9):
                if self.board_removal[row][col] != 0:
                    self.cells[row][col].text = str(self.board_removal[row][col])


class SudokuCell(TextInput):
    pass


class ErrorMessage(Label):
    pass


class WrongMessage(Label):
    pass


class CheckMessage(Label):
    pass


class SudokuApp(App):
    def build(self):
        self.title = 'Group 7 - Sudoku Game'
        return SudokuGame()


if __name__ == '__main__':
    SudokuApp().run()
